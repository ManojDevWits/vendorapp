import { StyleSheet } from "react-native"
import { Colors, fontScale, FontFamily, scale, globalStyle } from "../../theme";

const shadow = {
    backgroundColor: Colors.white,
    borderRadius: scale(10),
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 0,
    },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 3,
    borderWidth: 2.5,
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    innerContainer: {
        flex: 1,
        flexDirection: "column",
        padding: 20,
    },
    horizontalSpace: {
        paddingHorizontal: 20
    },
})

export default styles;