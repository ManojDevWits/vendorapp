import React, { useEffect, useState, useCallback, useMemo } from 'react'
import SplashScreen from 'react-native-splash-screen'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { View, Text, ScrollView, TouchableOpacity, FlatList } from 'react-native'
import { showToast } from '../../utils/Toast';
import StatusBar from '../../components/StatusBar/StatusBar';
import Header from '../../components/Header/Header';
import { Colors, FontFamily, fontScale, scale } from '../../theme';
import { AddWidth, AddHeight } from '../../components/AddSpace/AddSpace'
import styles from './MarketingCollateralPage.styles'
import CompletedOrdersIcon from '../../assets/images/img_completed_order.svg';
import PendingOrdersIcon from '../../assets/images/img_time.svg';
import DelayedOrdersIcon from '../../assets/images/img_clock.svg';
import CanceledOrdersIcon from '../../assets/images/img_cancel_order.svg';
import PageLabel from '../../components/PageLabel/PageLabel';
import InfoLabel from '../../components/InfoLabel/InfoLabel';
import ROUTES from '../../navigators/Routes';
import MarketingCollateralItem from '../../components/MarketingCollateralItem/MarketingCollateralItem';

const sampleData = [
    {
        id: 'ABXD43244',
        title: "GUIDES",
        url: 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png'
    },
    {
        id: 'ABXD43245',
        title: "BLOG",
        url: 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png'
    },
    {
        id: 'ABXD43246',
        title: "E-BOOKS",
        url: 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png'
    },
    {
        id: 'ABXD43247',
        title: "AUTHOR",
        url: 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png'
    },
    {
        id: 'ABXD43248',
        title: "NEWSLETTER",
        url: 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png'
    },
    {
        id: 'ABXD43249',
        title: "BROCHURE",
        url: 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png'
    },
    {
        id: 'ABXD432550',
        title: "GUIDES2",
        url: 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png'
    },
    {
        id: 'ABXD43251',
        title: "BLOG2",
        url: 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png'
    },
    {
        id: 'ABXD43252',
        title: "E-BOOKS2",
        url: 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png'
    },
    {
        id: 'ABXD43253',
        title: "AUTHOR2",
        url: 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png'
    },
    {
        id: 'ABXD43254',
        title: "NEWSLETTER2",
        url: 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png'
    },
    {
        id: 'ABXD43255',
        title: "BROCHURE2",
        url: 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png'
    }
]

const MarketingCollateralPage = () => {
    const [data, setData] = useState(sampleData)
    useEffect(() => {
        SplashScreen.hide()
    }, [])


    const renderRecord = (label, Icon, color, data) => {
        return (
            <View>
                <TouchableOpacity
                    // onPress={() => { }}
                    activeOpacity={0.9}
                    style={[styles.recordContainer, { borderColor: color, }]}>
                    <View style={styles.orderDataContainer}>
                        <Text
                            includeFontPadding={false}
                            style={
                                styles.recordDataText
                            }>{data + ''}</Text>
                        <Text style={
                            styles.recordLabelText
                        }>{label}</Text>
                    </View>
                    <View style={styles.recordIconContainer}>
                        <AddWidth size={scale(30)} />
                        <Icon height={scale(75)} width={scale(75)} />
                    </View>
                </TouchableOpacity>
            </View>

        )
    }

    return (
        <View style={styles.container}>
            <StatusBar color={Colors.statusbar} />
            <Header />
            <View style={{ margin: scale(20) }}>
                <PageLabel label={"JMR Collaterals and Docs"} />
            </View>
            {data && data.length && <FlatList
                style={styles.horizontalSpace}
                numColumns={2}
                showsVerticalScrollIndicator={false}
                data={data}
                keyExtractor={(item, index) => (index + item?.title)}
                renderItem={({ item, index }) => (
                    <MarketingCollateralItem
                        item={item}
                        index={index}
                        screen={ROUTES.MARKETING_COLLATERAL_PAGE}
                        onPress={()=>{}}
                    />
                )}
            />}

        </View >
    )
}

export default MarketingCollateralPage;