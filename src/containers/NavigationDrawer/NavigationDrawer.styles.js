import { StyleSheet } from "react-native"
import { Colors, fontScale, FontFamily, scale, globalStyle } from "../../theme";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.drawerBackground,
    },
    
    innerContainer: {
        flex: 1,
        flexDirection: "column",
        padding: 20,
    },
    closeButton: {
        alignSelf: 'flex-end',
        alignItems: 'center',
        justifyContent: 'center'
    },
    menuItemContainer:{
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: scale(22),
    },
    label:{
        color: Colors.white,
        fontFamily: FontFamily.Regular,
        fontSize: fontScale(15),
        flex: 1,
    },
    divider:{
        height: 2,
        backgroundColor: Colors.white
    },
    upperContainer:{
        paddingTop:scale(20),
        paddingHorizontal: scale(20),
    },
    nameContainer:{
        backgroundColor:Colors.drawerNameBackground,
        borderColor:Colors.white,
        borderWidth:0.2,
        borderRadius:5,
        marginTop:scale(26),
    },
    nameText:{
        color:Colors.white,
        // flex:1,
        marginVertical:scale(10),
        marginHorizontal:scale(20),
        fontSize:fontScale(17),
        fontFamily:FontFamily.Medium
    }
})

export default styles;