import React from 'react'
import { View, Text, ScrollView, TouchableOpacity, Platform, SafeAreaView } from 'react-native'
import StatusBar from '../../components/StatusBar/StatusBar'
import styles from './NavigationDrawer.styles'
import CloseIcon from '../../assets/icons/ic_close.svg'
import { Colors, scale, FontFamily, fontScale } from '../../theme'
import { AddHeight, AddWidth } from '../../components/AddSpace/AddSpace'
import DashboardIcon from '../../assets/icons/ic_dashboard.svg'
import CurrentOrdersIcon from '../../assets/icons/ic_order.svg'
import OrderHistoryIcon from '../../assets/icons/ic_order_history.svg'
import CancelledOrdersIcon from '../../assets/icons/ic_cancelled_order.svg'
import DelayIcon from '../../assets/icons/ic_delay.svg'
import ProductIcon from '../../assets/icons/ic_product.svg'
import CollateralIcon from '../../assets/icons/ic_collateral.svg'
import DocumentIcon from '../../assets/icons/ic_document.svg'
import AboutIcon from '../../assets/icons/ic_about.svg'
import NavigationService from '../../service/NavigationService'
import ROUTES from '../../navigators/Routes'
import { shallowEqual, useDispatch, useSelector } from "react-redux"

const iconSize = scale(22);
const NavigationDrawer = (props) => {
    const { name } = useSelector((state) => ({
        name: state.dataReducer.userData?.name,
    }), shallowEqual);

    const renderItem = (Icon, label, showDivider, callBack = () => { }) => {
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={callBack}
            >
                <View style={
                    styles.menuItemContainer
                }>
                    <Icon height={iconSize} width={iconSize} />
                    <AddWidth size={15} />
                    <Text
                        style={
                            styles.label
                        }>
                        {label}
                    </Text>
                </View>
                {showDivider && <View style={
                    styles.divider
                } />}
            </TouchableOpacity >

        )
    }

    const renderName = () => {
        return (
            <View style={
                styles.nameContainer
            }>
                <Text
                    style={
                        styles.nameText
                    }>{name ? name.toUpperCase() : "NA"}</Text>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <SafeAreaView />
            <View style={styles.upperContainer}>
                <TouchableOpacity
                    onPress={() => { props.navigation.closeDrawer() }}
                    style={
                        styles.closeButton
                    }>
                    <CloseIcon />
                </TouchableOpacity>
                {renderName()}
            </View>

            <ScrollView>
                <View style={styles.innerContainer}>
                    {renderItem(DashboardIcon, "Dashboard", true, () => {
                        NavigationService.navigate(ROUTES.DASHBOARD_PAGE);
                    })}
                    {renderItem(CurrentOrdersIcon, "Current Orders", true, () => {
                        NavigationService.navigate(ROUTES.CURRENT_ORDERS_PAGE);
                    })}
                    {renderItem(OrderHistoryIcon, "Order History", true, () => {
                        NavigationService.navigate(ROUTES.ORDER_HISTORY_PAGE);
                    })}
                    {renderItem(CancelledOrdersIcon, "Cancelled Orders", true, () => {
                        NavigationService.navigate(ROUTES.CANCELLED_ORDERS_PAGE);
                    })}
                    {renderItem(DelayIcon, "Order Delayed", true, () => {
                        NavigationService.navigate(ROUTES.ORDER_DELAYED_PAGE);
                    })}
                    {renderItem(ProductIcon, "Products", true, () => {

                    })}
                    {renderItem(CollateralIcon, "JMR collaterals and docs", true, () => {
                        // NavigationService.navigate(ROUTES.MARKETING_COLLATERAL_PAGE);
                    })}
                    {renderItem(DocumentIcon, "Vendor collaterals and docs", true, () => {
                        // NavigationService.navigate(ROUTES.COMPLIANCE_DOCUMENT_PAGE);
                    })}
                    {renderItem(AboutIcon, "About JMR", false, () => {

                    })}
                </View>
            </ScrollView>
        </View>
    )
}

export default NavigationDrawer;