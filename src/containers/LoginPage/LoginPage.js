import React, { useEffect, useCallback, useState } from 'react'
import { View, Text, Image, TextInput, TouchableWithoutFeedback, ScrollView, StyleSheet } from 'react-native'
import SplashScreen from 'react-native-splash-screen'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from './LoginPage.styles'
import LoginBanner from '../../assets/images/img_data_security.svg'
import PageLabel from '../../components/PageLabel/PageLabel'
import InfoLabel from '../../components/InfoLabel/InfoLabel'
import Button from '../../components/Button/Button'
import { Colors, FontFamily, fontScale, scale } from '../../theme'
import { AddHeight, AddWidth } from '../../components/AddSpace/AddSpace';
import { showToast } from '../../utils/Toast';
import StatusBar from '../../components/StatusBar/StatusBar';
import Header from '../../components/Header/Header';
import { shallowEqual, useDispatch, useSelector } from "react-redux"
import { callApiSaga, updateLoaderAction, updateLoginSessionAction, updateSplashAction } from '../../redux/actions';
import ROUTES from '../../navigators/Routes'
import { Constant } from '../../constants';
import Toast from 'react-native-root-toast';
import NavigationService from '../../service/NavigationService';



const LoginPage = () => {
    const dispatch = useDispatch();
    const [phoneNumber, setPhoneNumber] = useState(undefined);

    useEffect(() => {
        console.log("LOGIN PAGE")
        SplashScreen.hide()
    }, [])

    const onGetOtp = useCallback(
        () => {
            if (!phoneNumber || phoneNumber.length < 10) {
                showToast("Please enter valid mobile number");

            } else {
                // NavigationService.navigate(ROUTES.OTP_PAGE, { mobile: phoneNumber })
                let formData = new FormData();
                formData.append('mobile', phoneNumber)
                dispatch(callApiSaga({
                    params: null,
                    request: formData,
                    screen: ROUTES.LOGIN_PAGE,
                    apiType: Constant.ApiType.SEND_OTP,
                    successCallback: (res) => {
                        NavigationService.navigate(ROUTES.OTP_PAGE, { mobile: phoneNumber })
                    },
                    failureCallback: (res) => { }
                }))
            }
        },
        [phoneNumber],
    )

    return (
        <View style={styles.container}>
            <StatusBar />
            {/* <Header/> */}
            <KeyboardAwareScrollView
                contentContainerStyle={{ flex: 1 }}>
                <View style={
                    styles.innerContainer
                }>
                    <AddHeight size={20} />
                    <View style={styles.banner}>
                        <LoginBanner width={scale(234)} height={scale(186)} />
                    </View>
                    <AddHeight size={40} />
                    <PageLabel label={"Enter Your Mobile Number"} />
                    <AddHeight size={20} />
                    <InfoLabel label={"Please confirm your mobile number"} />
                    <AddHeight size={22} />
                    <Text style={
                        styles.inputLabel
                    }>Enter Mobile Number</Text>
                    <TextInput
                        style={
                            styles.textInput
                        }
                        value={phoneNumber}
                        placeholder="eg: 9555789456"
                        maxLength={10}
                        keyboardType={'phone-pad'}
                        selectionColor={Colors.primary}
                        placeholderTextColor={Colors.inputLabel}
                        onChangeText={text => {
                            let str = text;
                            text = str.replace(/[^0-9]/g, "");
                            setPhoneNumber(text)
                        }
                        }
                    />
                    <AddHeight size={54} />
                    <Button onPress={onGetOtp} label={"GET OTP"} />
                    <AddHeight size={24} />
                    <Text style={styles.termText}>
                        <Text style={styles.termsColor}
                            onPress={() => { }}>
                            Term & Condition
                        </Text>
                        <Text style={{ color: Colors.black }}>
                            {' And '}
                        </Text>
                        <Text style={styles.termsColor}
                            onPress={() => { }}>
                            Privacy Policy
                        </Text>
                    </Text>
                    <AddHeight size={20} />
                </View>
            </KeyboardAwareScrollView>
        </View >
    )
}

export default LoginPage;