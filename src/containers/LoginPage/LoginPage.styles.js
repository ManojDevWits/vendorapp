import { StyleSheet } from "react-native"
import { Colors, fontScale, FontFamily, scale, globalStyle } from "../../theme";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    innerContainer:{ 
        flex: 1, 
        flexDirection: "column", 
        padding: 20, 
    },
    banner: {
        alignItems:'center',
    },
    termText: {
        fontSize: fontScale(16),
        fontFamily: FontFamily.Medium,
        alignSelf: 'center'
    },
    termsColor: {
        color: Colors.terms
    },
    textInput:{
        ...globalStyle.defaultInputStyle,
         marginHorizontal: scale(24),
    },
    inputLabel:{
        color: Colors.inputLabel,
        fontFamily: FontFamily.Medium,
        marginHorizontal: scale(24),
        marginBottom: scale(-15),
    }
})

export default styles;