import { StyleSheet, PixelRatio } from "react-native"
import { Colors, fontScale, FontFamily, scale, globalStyle, scaleHeight } from "../../theme";

const shadow = {
    backgroundColor: Colors.white,
    borderRadius: scale(10),
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 0,
    },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 3,
    borderWidth: 2.5,
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    innerContainer: {
        flex: 1,
        flexDirection: "column",
        padding: 20,
    },
    recordContainer: {
        ...shadow,
        // height: scaleHeight(120),
        flexDirection: 'row',
    },
    orderDataContainer: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'space-between',
        paddingVertical: scale(15),
    },
    recordDataText: {
        height: fontScale(60),
        color: Colors.black,
        fontSize: fontScale(60),
        lineHeight: fontScale(70),
        fontFamily: FontFamily.Medium,
        flex:1
    },
    recordLabelText:{
        color: Colors.black,
        fontFamily: FontFamily.Medium,
        fontSize: fontScale(14),
    },
    recordIconContainer:{ 
        flex: 1, 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'flex-start', 
    }
})

export default styles;