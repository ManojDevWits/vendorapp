import React, { useEffect, useState, useCallback } from 'react'
import SplashScreen from 'react-native-splash-screen'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native'
import { showToast } from '../../utils/Toast';
import StatusBar from '../../components/StatusBar/StatusBar';
import Header from '../../components/Header/Header';
import { Colors, FontFamily, fontScale, scale } from '../../theme';
import { AddWidth, AddHeight } from '../../components/AddSpace/AddSpace'
import styles from './DashboardPage.styles'
import CompletedOrdersIcon from '../../assets/images/img_completed_order.svg';
import PendingOrdersIcon from '../../assets/images/img_time.svg';
import DelayedOrdersIcon from '../../assets/images/img_clock.svg';
import CanceledOrdersIcon from '../../assets/images/img_cancel_order.svg';
import PageLabel from '../../components/PageLabel/PageLabel';
import InfoLabel from '../../components/InfoLabel/InfoLabel';
import { shallowEqual, useDispatch, useSelector } from "react-redux"
import { callApiSaga, updateLoaderAction } from '../../redux/actions';
import { Constant } from '../../constants';
import ROUTES from '../../navigators/Routes';
import { useFocusEffect } from '@react-navigation/native';
import NavigationService from '../../service/NavigationService';


const DashboardPage = () => {
    const dispatch = useDispatch();
    const [completeOrder, setCompleteOrder] = useState(0)
    const [cancelOrder, setCancelOrder] = useState(0)
    const [pendingOrder, setPendingOrder] = useState(0)
    const [delayedOrder, setDelayedOrder] = useState(0)

    const { name } = useSelector((state) => ({
        name: state.dataReducer.userData?.name,
    }), shallowEqual);

    useFocusEffect(
        React.useCallback(() => {
            return orderCountApi();
        }, [])
    );

    useEffect(() => {
        SplashScreen.hide();
    }, [])

    const orderCountApi = useCallback(() => {
        // return;
        dispatch(callApiSaga({
            params: null,
            request: null,
            screen: ROUTES.DASHBOARD_PAGE,
            apiType: Constant.ApiType.DASHBOARD,
            successCallback: (res) => {
                if (res.data) {
                    const { data } = res;
                    setCompleteOrder(data?.CompleteOrderCount)
                    setCancelOrder(data?.CancelOrderCount)
                    setPendingOrder(data?.PendingOrderCount)
                    setDelayedOrder(data?.DelayedOrdersCount)
                }
            },
            failureCallback: (res) => { }
        }))
    }, [])


    const renderRecord = (label, Icon, color, data, onPress = () => { }) => {
        return (
            <View>
                <TouchableOpacity
                    onPress={onPress}
                    activeOpacity={0.7}
                    style={[styles.recordContainer, { borderColor: color, }]}>
                    <View style={styles.orderDataContainer}>
                        <Text
                            // adjustsFontSizeToFit
                            includeFontPadding={false}
                            style={
                                styles.recordDataText
                            }>{data + ''}</Text>
                        <Text style={
                            styles.recordLabelText
                        }>{label}</Text>
                    </View>
                    <View style={styles.recordIconContainer}>
                        <AddWidth size={scale(30)} />
                        <Icon height={scale(75)} width={scale(75)} />
                    </View>
                </TouchableOpacity>
            </View>

        )
    }

    return (
        <View style={styles.container}>
            <StatusBar color={Colors.statusbar} />
            <Header />
            <ScrollView
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={{ flexGrow: 1 }}>
                <View style={
                    styles.innerContainer
                }>
                    <InfoLabel label={`Welcome, ${name}`} />
                    <AddHeight size={4} />
                    <PageLabel label={"Your Dashboard"} />
                    <AddHeight size={20} />
                    {renderRecord("Completed Orders", CompletedOrdersIcon, Colors.completedOrders, completeOrder, () => {
                        NavigationService.navigate(ROUTES.ORDER_HISTORY_PAGE);
                    })}
                    <AddHeight size={40} />
                    {renderRecord("Pending Orders", PendingOrdersIcon, Colors.pendingOrders, pendingOrder, () => {
                        NavigationService.navigate(ROUTES.CURRENT_ORDERS_PAGE);
                    })}
                    <AddHeight size={40} />
                    {renderRecord("Delayed Orders", DelayedOrdersIcon, Colors.delayedOrders, delayedOrder,() => {
                        NavigationService.navigate(ROUTES.ORDER_DELAYED_PAGE);
                    })}
                    <AddHeight size={40} />
                    {renderRecord("Canceled Orders", CanceledOrdersIcon, Colors.canceledOrders, cancelOrder,() => {
                        NavigationService.navigate(ROUTES.CANCELLED_ORDERS_PAGE);
                    })}
                </View>
            </ScrollView>
        </View >
    )
}

export default DashboardPage;