import React, { useCallback, useEffect, useState } from 'react'
import { View, Text, Image } from 'react-native'
import { shallowEqual, useDispatch, useSelector, useEff } from "react-redux"
import logo from '../../assets/images/img_logo.png'
import { Colors } from '../../theme'
import SplashScreen from 'react-native-splash-screen'
import { Constant } from '../../constants'
import { updateSplashAction, updateLoginSessionAction, callApiSaga } from '../../redux/actions'
import ROUTES from '../../navigators/Routes'


const SplashPage = () => {
    const dispatch = useDispatch();
    const { token } = useSelector((state) => ({
        token: state.dataReducer.token,
    }), shallowEqual);

    useEffect(() => {
        // SplashScreen.hide();
        console.log("SPLASH_PAGE")
        // dispatch(updateLoginSessionAction(true))
        // dispatch(updateSplashAction(false))
        // onFailure();
        // return;
        if (token) {
            dispatch(callApiSaga({
                params: null,
                request: null,
                screen: ROUTES.SPLASH_PAGE,
                apiType: Constant.ApiType.VERIFY_TOKEN,
                successCallback: (res) => {
                    dispatch(updateLoginSessionAction(true))
                    dispatch(updateSplashAction(false))
                },
                failureCallback: (res) => {
                    onFailure();
                }
            }))
        } else {
            onFailure();
        }
    }, [])

    const onFailure = () => {
        dispatch(updateSplashAction(false))
        dispatch(updateLoginSessionAction(false))
    }

    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: "center", backgroundColor: Colors.white }}>
            {/* <Text>Splash</Text> */}
            <Image source={logo}></Image>
        </View>
    )
}


export default SplashPage