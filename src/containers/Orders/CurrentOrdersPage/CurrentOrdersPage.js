import React, { useEffect, useState, useCallback, useRef } from 'react'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { View, Text, ScrollView, TouchableOpacity, FlatList } from 'react-native'
import { showToast } from '../../../utils/Toast';
import StatusBar from '../../../components/StatusBar/StatusBar';
import Header from '../../../components/Header/Header';
import { Colors, FontFamily, fontScale, scale } from '../../../theme';
import { AddWidth, AddHeight } from '../../../components/AddSpace/AddSpace'
import styles from './CurrentOrdersPage.styles'
import CompletedOrdersIcon from '../../../assets/images/img_completed_order.svg';
import PendingOrdersIcon from '../../../assets/images/img_time.svg';
import DelayedOrdersIcon from '../../../assets/images/img_clock.svg';
import CanceledOrdersIcon from '../../../assets/images/img_cancel_order.svg';
import PageLabel from '../../../components/PageLabel/PageLabel';
import InfoLabel from '../../../components/InfoLabel/InfoLabel';
import OrderItem from '../../../components/OrderItem/OrderItem';
import ROUTES from '../../../navigators/Routes';
import SearchFilter from '../../../components/SearchFilter/SearchFilter';
import BottomSheet from '../../../components/BottomSheet/BottomSheet';
import Button from '../../../components/Button/Button';
import FilterCurrentOrder from '../FilterOrder/FilterCurrentOrder';
import UpdateOrder from '../UpdateOrder/UpdateOrder';
import { cloneDeep } from 'lodash';
import RBSheet from 'react-native-raw-bottom-sheet';
import CurrentOrderItem from '../../../components/OrderItem/CurrentOrderItem';
import { useFocusEffect } from '@react-navigation/native';
import { callApiSaga, updateLoaderAction } from '../../../redux/actions';
import { Constant } from '../../../constants';
import { getOrderApiRequestForm } from '../../../utils';
import { shallowEqual, useDispatch, useSelector } from "react-redux"
import moment from 'moment';


const sampleData = [
    // {
    //     orderID: 'ABXD43244',
    //     sale_item_id: "27190",
    //     orderStatus: 'Pending',
    //     productname: "Paneer Tikka",
    //     productimage: 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png',
    //     productqty: 2,
    //     actual_pickup_date_time: '1585843992',
    //     acknowledgment_date_time: '1585679400',
    //     orderDate: "01-04-2020",
    // },
    // {
    //     orderID: 'ABXD43245',
    //     sale_item_id: "27191",
    //     orderStatus: 'Pending',
    //     productname: "Paneer Tikka",
    //     productimage: 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png',
    //     productqty: 2,
    //     actual_pickup_date_time: null,
    //     acknowledgment_date_time: null,
    //     orderDate: "01-04-2020",

    // },
    // {
    //     orderID: 'ABXD43246',
    //     sale_item_id: "27192",
    //     orderStatus: 'Pending',
    //     productname: "Paneer Tikka",
    //     productimage: 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png',
    //     productqty: 2,
    //     actual_pickup_date_time: '1585843992',
    //     acknowledgment_date_time: '1585679400',
    //     orderDate: "01-04-2020",
    // },
    // {
    //     orderID: 'ABXD43245',
    //     sale_item_id: "27193",
    //     orderStatus: 'Pending',
    //     productname: "Paneer Tikka",
    //     productimage: 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png',
    //     productqty: 2,
    //     actual_pickup_date_time: '1585843992',
    //     acknowledgment_date_time: '1585679400',
    //     orderDate: "01-04-2020",
    // },
    {
        "orderID": "SO20210430917",
        "sale_item_id": "59078",
        "orderStatus": Constant.OrderStatus.PENDING_ACK,
        "productname": "paneer-kochuri-with-aloor-tarkari-tasty-corner",
        "productimage": "0",
        "productqty": "1",
        "actual_pickup_date_time": null,
        "acknowledgment_date_time": null,
        "orderDate": "29-04-2021"
    },
    {
        "orderID": "SO20210430905",
        "sale_item_id": "59065",
        "orderStatus": Constant.OrderStatus.PENDING_ACK,
        "productname": "the-nostalgia-box",
        "productimage": "0",
        "productqty": "1",
        "actual_pickup_date_time": null,
        "acknowledgment_date_time": null,
        "orderDate": "14-04-2021"
    },
    {
        "orderID": "SO20210330894",
        "sale_item_id": "59051",
        "orderStatus": Constant.OrderStatus.PENDING_ACK,
        "productname": "cake-delivery-non-perishable-1-kg-volume",
        "productimage": "0",
        "productqty": "1",
        "actual_pickup_date_time": null,
        "acknowledgment_date_time": null,
        "orderDate": "20-03-2021"
    },
    {
        "orderID": "SO20210330880",
        "sale_item_id": "59037",
        "orderStatus": Constant.OrderStatus.PENDING_ACK,
        "productname": "mutton-curry-cut-1kg",
        "productimage": "0",
        "productqty": "1",
        "actual_pickup_date_time": null,
        "acknowledgment_date_time": null,
        "orderDate": "15-03-2021"
    },
    {
        "orderID": "SO20210330762",
        "sale_item_id": "58907",
        "orderStatus": "Paid",
        "productname": "the-nostalgia-box",
        "productimage": "0",
        "productqty": "2",
        "actual_pickup_date_time": null,
        "acknowledgment_date_time": null,
        "orderDate": "14-03-2021"
    },
    {
        "orderID": "SO20210330705",
        "sale_item_id": "58850",
        "orderStatus": Constant.OrderStatus.PENDING_PICKUP,
        "productname": "mutton-curry-cut-1kg",
        "productimage": "0",
        "productqty": "1",
        "actual_pickup_date_time": null,
        "acknowledgment_date_time": '1626971580',
        "orderDate": "14-03-2021"
    },
    {
        "orderID": "SO20210330700",
        "sale_item_id": "58845",
        "orderStatus": Constant.OrderStatus.PENDING_PICKUP,
        "productname": "mutton-curry-cut-1kg",
        "productimage": "0",
        "productqty": "1",
        "actual_pickup_date_time": null,
        "acknowledgment_date_time": '1585679400',
        "orderDate": "14-03-2021"
    },
    {
        "orderID": "SO20210330677",
        "sale_item_id": "58822",
        "orderStatus": Constant.OrderStatus.PENDING_PICKUP,
        "productname": "cut-echor-dressed",
        "productimage": "0",
        "productqty": "5",
        "actual_pickup_date_time": null,
        "acknowledgment_date_time": '1585679400',
        "orderDate": "14-03-2021"
    },
    {
        "orderID": "SO20210330676",
        "sale_item_id": "58821",
        "orderStatus": Constant.OrderStatus.PENDING_PICKUP,
        "productname": "cut-echor-dressed",
        "productimage": "0",
        "productqty": "4",
        "actual_pickup_date_time": null,
        "acknowledgment_date_time": '1585679400',
        "orderDate": "14-03-2021"
    }
]
let lockApiCall = false;
// const defaultFilterData = { startDate: null, endDate: null, status: null, searchText: null }
const CurrentOrdersPage = () => {
    const dispatch = useDispatch();
    const flatListRef = useRef(null)
    const [showFilter, setShowFilter] = useState(false)
    const [filterData, setFilterData] = useState(Constant.defaultFilterData)
    const [showOrderUpdate, setShowOrderUpdate] = useState(false)
    const [orderList, setOrderList] = useState(sampleData)
    const [itemToUpdate, setItemToUpdate] = useState(null)
    const [pageStart, setPageStart] = useState(1)


    useEffect(() => {
        return () => {
            console.log("********************** CLEAN_UP **********************");
        };
    }, [])

    useFocusEffect(
        React.useCallback(() => {
            cleanPage();
            return callCurrentOrderApi(1);
        }, [])
    );

    const callCurrentOrderApi = useCallback((page) => {
        if (lockApiCall) {
            return;
        }
        lockApiCall = true;
        console.log("FILTERS:" + JSON.stringify(filterData))
        dispatch(callApiSaga({
            params: null,
            request: getOrderApiRequestForm({
                start: page,
                startDate: filterData?.startDate,
                endDate: filterData?.endDate,
                status: filterData?.status,
                productName: filterData.searchText,
            }),
            screen: ROUTES.CURRENT_ORDERS_PAGE,
            apiType: Constant.ApiType.MY_ORDERS,
            successCallback: (res) => {
                lockApiCall = false;
                if (res.data) {
                    const { data } = res;
                    setPageStart(page)
                    if (page === 1) {
                        setOrderList(prev => data);
                    } else if (page > 1) {
                        setOrderList(prev => [...prev, ...data])
                    }
                }
            },
            failureCallback: (res) => {
                lockApiCall = false;
            }
        }))
    }, [orderList, pageStart, filterData])

    const callUpdateAckDateTimeApi = useCallback((item, index) => {
        let formData = new FormData();
        const currentTime = moment().unix().toString();
        formData.append('datetime', currentTime);
        formData.append('sale_item_id', item?.sale_item_id);
        dispatch(callApiSaga({
            params: null,
            request: formData,
            screen: ROUTES.CURRENT_ORDERS_PAGE,
            apiType: Constant.ApiType.UPDATE_ACK_DATE_TIME,
            successCallback: (res) => {
                if (res.data) {
                    const { data } = res;
                    //if data updated then make change in item in order list
                    if (data.updated) {
                        let orderList = cloneDeep(orderList);
                        item['acknowledgment_date_time'] = currentTime;
                        item['orderStatus'] = Constant.OrderStatus.PENDING_PICKUP;
                        orderList[index] = item;
                        setOrderList(orderList)
                    }
                }
            },
            failureCallback: (res) => {

            }
        }))
    }, [showOrderUpdate, orderList])

    const callUpdatePickupDateTimeApi = useCallback((item, time) => {
        let formData = new FormData();
        formData.append('datetime', time);
        formData.append('sale_item_id', item?.sale_item_id);
        dispatch(callApiSaga({
            params: null,
            request: formData,
            screen: ROUTES.CURRENT_ORDERS_PAGE,
            apiType: Constant.ApiType.UPDATE_PICKUP_DATE_TIME,
            successCallback: (res) => {
                if (res.data) {
                    const { data } = res;
                    //if data updated then make change in item in order list
                    if (data.updated) {
                        let orderList = cloneDeep(orderList);
                        const filteredList = orderList.filter(ele => ele.sale_item_id != item.sale_item_id)
                        // item['actual_pickup_date_time'] = currentTime;
                        // orderList[index] = item;
                        setOrderList(filteredList)
                    }
                }
            },
            failureCallback: (res) => {

            }
        }))
    }, [showOrderUpdate, orderList])

    const cleanPage = useCallback(
        () => {
            setOrderList([]);
            lockApiCall = false;
            // setOrderList(sampleData)
            setFilterData(Constant.defaultFilterData)
        },
        [],
    )

    const onCloseFilter = useCallback(
        () => {
            console.log("CLOSE FILTER")
            setShowFilter(false)
        },
        [showFilter],
    )

    const onCloseOrderUpdate = useCallback(
        () => {
            setShowOrderUpdate(false),
                setItemToUpdate(null);
        },
        [showOrderUpdate],
    )

    const onPressEdit = useCallback(
        (item, index) => {
            setItemToUpdate({ 'item': item, 'index': index })
            setShowOrderUpdate(true);
        },
        [orderList, itemToUpdate],
    )

    useEffect(() => {
        console.log("CALLED_FILTER_EFFECT")
        if (flatListRef != null && orderList.length > 0) {
            flatListRef.current.scrollToOffset({ animated: true, offset: 0 });
        }
        callCurrentOrderApi(1);
    }, [filterData])

    const onFiltered = useCallback(
        (startDate, endDate, status) => {
            if (filterData.startDate != startDate
                || filterData.endDate != endDate
                || filterData.status != status) {
                setFilterData(prev => ({
                    ...prev,
                    'startDate': startDate,
                    'endDate': endDate,
                    'status': status,
                }))
            }

            // setSelectedStartDate(startDate);
            // setSelectedEndDate(endDate);
            // setSelectedStatus(status);
            setShowFilter(false)
            // console.log("ON_FILTERS_AFTER")
            // console.log("START_DATE:" + selectedStartDate)
            // console.log("END_DATE:" + selectedEndDate)
            // callCurrentOrderApi(1);
        },
        [showFilter, filterData],
    )

    const onSearchText = useCallback(
        (text) => {
            if (filterData.searchText != text) {
                setFilterData(prev => ({
                    ...prev,
                    'searchText': text
                }))
            }
        },
        [filterData],
    )

    const onSearchClear = useCallback(
        () => {
            if (filterData
                && filterData.searchText
                && filterData.searchText != null
                && filterData.searchText != '') {
                setFilterData(prev => ({
                    ...prev,
                    'searchText': null
                }))
            }
        },
        [filterData],
    )

    // const onUpdateOrder = useCallback(
    //     (item, index, selectedStatus, pickupDate, pickupTime) => {
    //         //update order data based on order id
    //         let orderList = cloneDeep(orderList);
    //         let changedItem = cloneDeep(item);
    //         if(changedItem)
    //         setShowOrderUpdate(false)
    //     },
    //     [showFilter],
    // )

    const onUpdateAck = useCallback(
        (item, index, selectedStatus) => {
            //update order data based on order id
            setShowOrderUpdate(false)
            setItemToUpdate(null);
            if (selectedStatus != null) {
                callUpdateAckDateTimeApi(item, index)
            }
        },
        [showOrderUpdate],
    )

    const onUpdatePickup = useCallback(
        (item, index, pickupDate, pickupTime) => {
            //update order data based on order id
            setShowOrderUpdate(false)
            setItemToUpdate(null);
            if (pickupDate != null && pickupTime != null) {
                const time = moment(moment(pickupDate + ' ' + pickupTime, Constant.DateFormat.DD_MM_YYYY_HH_MM_A).unix());
                callUpdatePickupDateTimeApi(item, time)
            }
        },
        [showOrderUpdate],
    )

    return (
        <View style={styles.container}>
            <StatusBar color={Colors.statusbar} />
            <Header />
            <View style={styles.innerContainer}>
                <View style={
                    styles.horizontalSpace
                }>
                    <PageLabel label={"Current Orders"} />
                    <AddHeight size={20} />
                    <SearchFilter
                        onChangeText={() => { }}
                        onPressFilter={() => {
                            setShowFilter(true)
                        }}
                        onSubmit={onSearchText}
                        onSearchClear={onSearchClear}
                    />
                    <AddHeight size={20} />

                </View>
                {orderList && orderList.length > 0 && <FlatList
                    ref={flatListRef}
                    style={[{ flex: 1 }, styles.horizontalSpace]}
                    showsVerticalScrollIndicator={false}
                    data={orderList}
                    keyExtractor={(item, index) => (index + item?.sale_item_id)}
                    removeClippedSubviews={true}
                    maxToRenderPerBatch={60}
                    windowSize={30}
                    onEndReachedThreshold={0}
                    onEndReached={() => { callCurrentOrderApi(pageStart + 1) }}
                    renderItem={({ item, index }) => (
                        <CurrentOrderItem
                            item={item}
                            index={index}
                            screen={ROUTES.CURRENT_ORDERS_PAGE}
                            onPressEdit={onPressEdit}
                            onPressTime={() => { }}
                        />
                    )}
                />}

            </View>
            <FilterCurrentOrder
                visible={showFilter}
                startDate={filterData?.startDate}
                endDate={filterData?.endDate}
                status={filterData?.status}
                onClose={onCloseFilter}
                onPress={onFiltered} />
            <UpdateOrder
                visible={showOrderUpdate}
                item={itemToUpdate?.item}
                index={itemToUpdate?.index}
                onClose={onCloseOrderUpdate}
                // onPress={onUpdateOrder}
                updateAck={onUpdateAck}
                updatePickup={onUpdatePickup}
            />
        </View >
    )
}

export default CurrentOrdersPage;