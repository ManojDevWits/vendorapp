import { StyleSheet } from "react-native"
import { Colors, fontScale, FontFamily, scale, globalStyle } from "../../../theme";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    innerContainer: {
        flex: 1,
        flexDirection: "column",
        paddingTop: scale(20)
    },
    horizontalSpace: {
        paddingHorizontal: 20
    },
})

export default styles;