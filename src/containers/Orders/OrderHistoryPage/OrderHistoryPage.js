import React, { useEffect, useState, useCallback, useRef } from 'react'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { View, Text, ScrollView, TouchableOpacity, FlatList } from 'react-native'
import { showToast } from '../../../utils/Toast';
import StatusBar from '../../../components/StatusBar/StatusBar';
import Header from '../../../components/Header/Header';
import { Colors, FontFamily, fontScale, scale } from '../../../theme';
import { AddWidth, AddHeight } from '../../../components/AddSpace/AddSpace'
import styles from './OrderHistoryPage.styles'
import CompletedOrdersIcon from '../../../assets/images/img_completed_order.svg';
import PendingOrdersIcon from '../../../assets/images/img_time.svg';
import DelayedOrdersIcon from '../../../assets/images/img_clock.svg';
import CanceledOrdersIcon from '../../../assets/images/img_cancel_order.svg';
import PageLabel from '../../../components/PageLabel/PageLabel';
import InfoLabel from '../../../components/InfoLabel/InfoLabel';
import OrderItem from '../../../components/OrderItem/OrderItem';
import ROUTES from '../../../navigators/Routes';
import SearchFilter from '../../../components/SearchFilter/SearchFilter';
import BottomSheet from '../../../components/BottomSheet/BottomSheet';
import Button from '../../../components/Button/Button';
import FilterCurrentOrder from '../FilterOrder/FilterCurrentOrder';
import { cloneDeep } from 'lodash';
import { useFocusEffect } from '@react-navigation/native';
import { callApiSaga, updateLoaderAction } from '../../../redux/actions';
import { Constant } from '../../../constants';
import { getOrderApiRequestForm } from '../../../utils';
import { shallowEqual, useDispatch, useSelector } from "react-redux"
import moment from 'moment';
import OrderHistoryItem from '../../../components/OrderItem/OrderHistoryItem';
import FilterOrderHistory from '../FilterOrder/FilterOrderHistory';


const sampleData = [
    {
        "orderID": "SO20210430917",
        "sale_item_id": "59078",
        "orderStatus": Constant.OrderStatus.PENDING_ACK,
        "productname": "paneer-kochuri-with-aloor-tarkari-tasty-corner",
        "productimage": 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png',
        "productqty": "1",
        "actual_pickup_date_time": '1585679400',
        "acknowledgment_date_time": '1585679400',
        "orderDate": "29-04-2021"
    },
    {
        "orderID": "SO20210430905",
        "sale_item_id": "59065",
        "orderStatus": Constant.OrderStatus.PENDING_ACK,
        "productname": "the-nostalgia-box",
        "productimage": "0",
        "productqty": "1",
        "actual_pickup_date_time": '1585679400',
        "acknowledgment_date_time": '1585679400',
        "orderDate": "14-04-2021"
    },
    {
        "orderID": "SO20210330894",
        "sale_item_id": "59051",
        "orderStatus": Constant.OrderStatus.PENDING_ACK,
        "productname": "cake-delivery-non-perishable-1-kg-volume",
        "productimage": "0",
        "productqty": "1",
        "actual_pickup_date_time": '1585679400',
        "acknowledgment_date_time": '1585679400',
        "orderDate": "20-03-2021"
    },
    {
        "orderID": "SO20210330880",
        "sale_item_id": "59037",
        "orderStatus": Constant.OrderStatus.PENDING_ACK,
        "productname": "mutton-curry-cut-1kg",
        "productimage": "0",
        "productqty": "1",
        "actual_pickup_date_time": '1585679400',
        "acknowledgment_date_time": '1585679400',
        "orderDate": "15-03-2021"
    },
    {
        "orderID": "SO20210330762",
        "sale_item_id": "58907",
        "orderStatus": "Paid",
        "productname": "the-nostalgia-box",
        "productimage": "0",
        "productqty": "2",
        "actual_pickup_date_time": '1585679400',
        "acknowledgment_date_time": '1585679400',
        "orderDate": "14-03-2021"
    },
    {
        "orderID": "SO20210330705",
        "sale_item_id": "58850",
        "orderStatus": Constant.OrderStatus.PENDING_PICKUP,
        "productname": "mutton-curry-cut-1kg",
        "productimage": "0",
        "productqty": "1",
        "actual_pickup_date_time": null,
        "acknowledgment_date_time": '1626971580',
        "orderDate": "14-03-2021"
    },
    {
        "orderID": "SO20210330700",
        "sale_item_id": "58845",
        "orderStatus": Constant.OrderStatus.PENDING_PICKUP,
        "productname": "mutton-curry-cut-1kg",
        "productimage": "0",
        "productqty": "1",
        "actual_pickup_date_time": null,
        "acknowledgment_date_time": '1585679400',
        "orderDate": "14-03-2021"
    },
    {
        "orderID": "SO20210330677",
        "sale_item_id": "58822",
        "orderStatus": Constant.OrderStatus.PENDING_PICKUP,
        "productname": "cut-echor-dressed",
        "productimage": "0",
        "productqty": "5",
        "actual_pickup_date_time": null,
        "acknowledgment_date_time": '1585679400',
        "orderDate": "14-03-2021"
    },
    {
        "orderID": "SO20210330676",
        "sale_item_id": "58821",
        "orderStatus": Constant.OrderStatus.PENDING_PICKUP,
        "productname": "cut-echor-dressed",
        "productimage": "0",
        "productqty": "4",
        "actual_pickup_date_time": null,
        "acknowledgment_date_time": '1585679400',
        "orderDate": "14-03-2021"
    }
]
let lockApiCall = false;
// const defaultFilterData = { startDate: null, endDate: null, status: null, searchText: '' }
const OrderHistoryPage = () => {
    const dispatch = useDispatch();
    const flatListRef = useRef(null)
    const [showFilter, setShowFilter] = useState(false)
    const [filterNumber, setFilterNumber] = useState(null)
    const [filterData, setFilterData] = useState(Constant.defaultFilterData)
    const [orderList, setOrderList] = useState([])
    const [pageStart, setPageStart] = useState(1)


    useEffect(() => {
        return () => {
            console.log("********************** CLEAN_UP **********************");
        };
    }, [])

    useFocusEffect(
        React.useCallback(() => {
            cleanPage();
            return callOrderApi(1);
        }, [])
    );

    const callOrderApi = useCallback((page) => {
        // return;
        if (lockApiCall) {
            return;
        }
        lockApiCall = true;
        console.log("FILTERS:" + JSON.stringify(filterData))
        dispatch(callApiSaga({
            params: null,
            request: getOrderApiRequestForm({
                start: page,
                startDate: filterData?.startDate,
                endDate: filterData?.endDate,
                productName: filterData.searchText,
            }),
            screen: ROUTES.ORDER_HISTORY_PAGE,
            apiType: Constant.ApiType.HISTORY_ORDERS,
            successCallback: (res) => {
                lockApiCall = false;
                if (res.data) {
                    const { data } = res;
                    setPageStart(page)
                    if (page === 1) {
                        setOrderList(prev => data);
                    } else if (page > 1) {
                        setOrderList(prev => [...prev, ...data])
                    }
                }
            },
            failureCallback: (res) => {
                lockApiCall = false;
            }
        }))
    }, [orderList, pageStart, filterData])

    const cleanPage = useCallback(
        () => {
            setOrderList([]);
            // setOrderList(sampleData);
            lockApiCall = false;
            setFilterData(Constant.defaultFilterData)
        },
        [],
    )

    const onCloseFilter = useCallback(
        () => {
            console.log("CLOSE FILTER IN ORDER HIS PAGE")
            setShowFilter(false)
        },
        [showFilter],
    )

    useEffect(() => {
        console.log("CALLED_FILTER_EFFECT")
        if (flatListRef != null && orderList.length > 0) {
            flatListRef.current.scrollToOffset({ animated: true, offset: 0 });
        }
        callOrderApi(1);
    }, [filterData])

    const onFiltered = useCallback(
        (startDate, endDate, status) => {
            if (filterData.startDate != startDate
                || filterData.endDate != endDate
                || filterNumber != status) {
                setFilterData(prev => ({
                    ...prev,
                    'startDate': startDate,
                    'endDate': endDate,
                }))
                setFilterNumber(status);
            }
            setShowFilter(false)
        },
        [showFilter, filterData, filterNumber],
    )

    const onSearchText = useCallback(
        (text) => {
            if (filterData.searchText != text) {
                setFilterData(prev => ({
                    ...prev,
                    'searchText': text
                }))
            }
        },
        [filterData],
    )

    const onSearchClear = useCallback(
        () => {
            if (filterData
                && filterData.searchText
                && filterData.searchText != null
                && filterData.searchText != '') {
                setFilterData(prev => ({
                    ...prev,
                    'searchText': null
                }))
            }
        },
        [filterData],
    )

    return (
        <View style={styles.container}>
            <StatusBar color={Colors.statusbar} />
            <Header />
            <View style={styles.innerContainer}>
                <View style={
                    styles.horizontalSpace
                }>
                    <PageLabel label={"Order History"} />
                    <AddHeight size={20} />
                    <SearchFilter
                        onChangeText={() => { }}
                        onPressFilter={() => {
                            setShowFilter(true)
                        }}
                        onSubmit={onSearchText}
                        onSearchClear={onSearchClear}
                    />
                    <AddHeight size={20} />

                </View>
                {orderList && orderList.length > 0 && <FlatList
                    ref={flatListRef}
                    style={[{ flex: 1 }, styles.horizontalSpace]}
                    showsVerticalScrollIndicator={false}
                    data={orderList}
                    keyExtractor={(item, index) => (index + item?.sale_item_id)}
                    removeClippedSubviews={true}
                    maxToRenderPerBatch={60}
                    windowSize={30}
                    onEndReachedThreshold={0}
                    onEndReached={() => { callOrderApi(pageStart + 1) }}
                    renderItem={({ item, index }) => (
                        <OrderHistoryItem
                            item={item}
                            index={index}
                            screen={ROUTES.ORDER_HISTORY_PAGE}
                        />
                    )}
                />}

            </View>
            <FilterOrderHistory
                visible={showFilter}
                startDate={filterNumber === 4 ? filterData?.startDate : null}
                endDate={filterNumber === 4 ? filterData?.endDate : null}
                status={filterNumber}
                onClose={onCloseFilter}
                onPress={onFiltered} />
        </View >
    )
}

export default OrderHistoryPage;