import React, { useEffect, useState, useCallback, useRef } from 'react'
import { View, Text, ScrollView, TouchableOpacity, FlatList } from 'react-native'
import { Colors, FontFamily, fontScale, scale } from '../../../theme';
import { AddWidth, AddHeight } from '../../../components/AddSpace/AddSpace'
import styles from './FilterOrder.styles'
import CalendarIcon from '../../../assets/icons/ic_calendar.svg';
import SelectedIcon from '../../../assets/icons/ic_select.svg';
import UnselectedIcon from '../../../assets/icons/ic_select_grey.svg';
import BottomSheet from '../../../components/BottomSheet/BottomSheet';
import Button from '../../../components/Button/Button';
import { Constant } from '../../../constants/constant';
import Calendar from '../../../components/Calendar/Calendar';
import RBSheet from "react-native-raw-bottom-sheet";
import moment from 'moment';
import RadioButton from '../../../components/RadioButton/RadioButton';


const radioIconSize = scale(22);
const FilterCurrentOrder = ({
    visible,
    startDate,
    endDate,
    status,
    onClose,
    onPress
}) => {
    const sheetRef = useRef(null)
    const [height, setHeight] = useState(scale(460));
    const [selectedStartDate, setSelectedStartDate] = useState(startDate)
    const [selectedEndDate, setSelectedEndDate] = useState(endDate)
    const [selectedStatus, setSelectedStatus] = useState(status)
    const [showCalendar, setShowCalendar] = useState(false)

    const onCloseCalendar = useCallback(
        () => {
            setShowCalendar(false);
        },
        [showCalendar],
    )

    useEffect(() => {
        if (sheetRef) {
            if (visible) {
                sheetRef.current.open()
            } else {
                sheetRef.current.close()
            }
        } else {
            console.log("NO SHEET")
        }
    }, [visible])

    const getDateToShow = (date, isEndDate) => {
        if (date && date != null && date != "") {
            const dateType = moment(date, Constant.DateFormat.DD_MM_YYYY);
            return moment(dateType).format(Constant.DateFormat.DD_MMM_YYYY).toString();
        } else if (isEndDate) {
            return 'End Date'
        } else {
            return 'Start Date'
        }
    }

    const onDateRangeSelected = useCallback(
        (startDate, endDate) => {
            setSelectedStartDate(startDate)
            setSelectedEndDate(endDate)
            setShowCalendar(false)
        },
        [showCalendar],
    )

    const onSort = useCallback(
        () => {
            onPress(selectedStartDate, selectedEndDate, selectedStatus)
        },
        [selectedStartDate, selectedEndDate, selectedStatus, showCalendar],
    )

    const onClearFilter = useCallback(
        () => {
            setSelectedStartDate(null)
            setSelectedEndDate(null)
            setSelectedStatus(null)
        }
    )

    // const RadioStatus = ({ label, value }) => {
    //     return (
    //         <TouchableOpacity
    //             onPress={() => { setSelectedStatus(prev => prev === value ? null : value) }}
    //             style={{
    //                 flexDirection: 'row',
    //                 alignItems: 'center',
    //             }}>
    //             {value === selectedStatus ? <SelectedIcon height={scale(radioIconSize)} width={scale(radioIconSize)} /> : <UnselectedIcon height={scale(radioIconSize)} width={scale(radioIconSize)} />}
    //             <AddWidth size={10} />
    //             <Text style={styles.normalLabel}>{label}</Text>
    //         </TouchableOpacity>
    //     )
    // }

    return (
        <RBSheet
            ref={sheetRef}
            openDuration={250}
            customStyles={{
                container: {...styles.bottomSheetContainer,...{ height: height }},
                draggableIcon: styles.draggableIcon
            }}
            closeOnDragDown={true}
            closeOnPressMask={true}
            onClose={onClose}
            keyboardAvoidingViewEnabled={true}
        >
            <View style={styles.container} onLayout={(e) => setHeight(e.nativeEvent.layout.height)}>
                <Text style={
                    styles.headerText
                }>{"Filter"}</Text>
                <View style={styles.clearContainer}>
                    <Text style={
                        styles.headerLabel
                    }>{'Select Start & End Date'}</Text>
                    <TouchableOpacity
                        onPress={onClearFilter}>
                        <Text
                            style={
                                styles.clearText
                            }
                        >{'Clear All'}</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity
                    onPress={() => { setShowCalendar(true) }}
                    style={styles.calendarButton}
                >
                    <CalendarIcon height={scale(27)} width={scale(27)} />
                    <View style={styles.pipe} />
                    <Text style={
                        [styles.normalLabel, styles.calendarLabel]
                    }>{`${getDateToShow(selectedStartDate)} - ${getDateToShow(selectedEndDate, true)}`}</Text>
                </TouchableOpacity>
                <AddHeight size={30} />
                <Text style={
                    styles.headerLabel
                }>{'Sort by Status'}</Text>
                <View style={styles.radioContainer}>
                    <View style={{
                        flex: 1,
                    }}>
                        {/* <RadioStatus
                            label={Constant.OrderStatus.PENDING_PICKUP}
                            value={Constant.OrderStatus.PENDING_PICKUP}
                        /> */}
                        <RadioButton
                            label={Constant.OrderStatus.PENDING_PICKUP}
                            value={Constant.OrderStatus.PENDING_PICKUP}
                            isSelected={selectedStatus === Constant.OrderStatus.PENDING_PICKUP}
                            onPress={() => { setSelectedStatus(prev => prev === Constant.OrderStatus.PENDING_PICKUP ? null : Constant.OrderStatus.PENDING_PICKUP) }}
                        />
                    </View>
                    <View style={{
                        flex: 1,
                    }}>
                        {/* <RadioStatus
                            label={'Pending Ack'}
                            value={Constant.OrderStatus.PENDING_ACK}
                        /> */}
                        <RadioButton
                            label={'Pending Ack'}
                            value={Constant.OrderStatus.PENDING_ACK}
                            isSelected={selectedStatus === Constant.OrderStatus.PENDING_ACK}
                            onPress={() => { setSelectedStatus(prev => prev === Constant.OrderStatus.PENDING_ACK ? null : Constant.OrderStatus.PENDING_ACK) }}
                        />
                    </View>
                </View>
                <AddHeight size={20} />
                <Button
                    onPress={onSort}
                    label={"SORT"}
                />
                <AddHeight size={25} />
            </View>
            <Calendar
                visible={showCalendar}
                allowRange={true}
                onClose={onCloseCalendar}
                onDateRangeSelected={onDateRangeSelected}
                startDate={selectedStartDate}
                endDate={selectedEndDate}
            />
        </RBSheet >
    )
}

FilterCurrentOrder.defaultProps = {
    visible: false,
    startDate: null,
    endDate: null,
    status: null,
    onClose: () => { },
    onPress: (startDate, endDate, status) => { },
}

export default FilterCurrentOrder;