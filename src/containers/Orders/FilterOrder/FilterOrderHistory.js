import React, { useEffect, useState, useCallback, useRef } from 'react'
import { View, Text, ScrollView, TouchableOpacity, FlatList } from 'react-native'
import { Colors, FontFamily, fontScale, scale } from '../../../theme';
import { AddWidth, AddHeight } from '../../../components/AddSpace/AddSpace'
import styles from './FilterOrder.styles'
import CalendarIcon from '../../../assets/icons/ic_calendar.svg';
import SelectedIcon from '../../../assets/icons/ic_select.svg';
import UnselectedIcon from '../../../assets/icons/ic_select_grey.svg';
import BottomSheet from '../../../components/BottomSheet/BottomSheet';
import Button from '../../../components/Button/Button';
import { Constant } from '../../../constants/constant';
import Calendar from '../../../components/Calendar/Calendar';
import RBSheet from "react-native-raw-bottom-sheet";
import moment from 'moment';
import RadioButton from '../../../components/RadioButton/RadioButton';


const radioIconSize = scale(22);
const FilterOrderHistory = ({
    visible,
    startDate,
    endDate,
    status,
    onClose,
    onPress
}) => {
    const sheetRef = useRef(null)
    const [isVisible, setVisible] = useState(visible);
    const [height, setHeight] = useState(scale(460));
    const [selectedStartDate, setSelectedStartDate] = useState(startDate)
    const [selectedEndDate, setSelectedEndDate] = useState(endDate)
    const [selectedStatus, setSelectedStatus] = useState(status)
    const [showCalendar, setShowCalendar] = useState(false)

    const onCloseCalendar = useCallback(
        () => {
            setShowCalendar(false);
        },
        [showCalendar],
    )

    useEffect(() => {
        console.log("CHECK_FILTER")
        if (sheetRef) {
            if (visible) {
                sheetRef.current.open()
                setVisible(true);
            }
            // else {
            //     sheetRef.current.close()
            // }
        } else {
            console.log("NO SHEET")
        }
    }, [visible])


    const closeSheet = useCallback(
        () => {
            console.log("CLOSE.....111")
            if (isVisible) {
                console.log("CLOSE.....222")
                setVisible(false)
                if (sheetRef) {
                    sheetRef.current.close();
                }
                onClose()
            }
        },
        [isVisible],
    )

    const getDateToShow = (date, isEndDate) => {
        if (date && date != null && date != "") {
            const dateType = moment(date, Constant.DateFormat.DD_MM_YYYY);
            return moment(dateType).format(Constant.DateFormat.DD_MMM_YYYY).toString();
        } else if (isEndDate) {
            return 'End Date'
        } else {
            return 'Start Date'
        }
    }

    const onDateRangeSelected = useCallback(
        (startDate, endDate) => {
            setSelectedStatus(4);
            setSelectedStartDate(startDate)
            setSelectedEndDate(endDate)
            setShowCalendar(false)
        },
        [showCalendar],
    )

    const onSort = useCallback(
        () => {
            let startDate = null;
            let endDate = null;
            switch (selectedStatus) {
                case 1:
                    startDate = moment().subtract(7, 'days').format(Constant.DateFormat.DD_MM_YYYY);
                    endDate = moment().format(Constant.DateFormat.DD_MM_YYYY);
                    break;
                case 2:
                    startDate = moment().subtract(15, 'days').format(Constant.DateFormat.DD_MM_YYYY);
                    endDate = moment().format(Constant.DateFormat.DD_MM_YYYY);
                    break;
                case 3:
                    startDate = moment().subtract(1, 'months').format(Constant.DateFormat.DD_MM_YYYY);
                    endDate = moment().format(Constant.DateFormat.DD_MM_YYYY);
                    break;
                case 4:
                    startDate = selectedStartDate;
                    endDate = selectedEndDate;
                    break;
            }
            closeSheet();
            onPress(startDate, endDate, selectedStatus)
        },
        [selectedStartDate, selectedEndDate, selectedStatus, showCalendar, closeSheet],
    )

    const onClearFilter = useCallback(
        () => {
            setSelectedStartDate(null)
            setSelectedEndDate(null)
            setSelectedStatus(null)
        }, [visible]
    )

    const onSelectFilterNum = useCallback(
        (num) => {
            console.log("NUM:" + num)

            switch (num) {
                case 1:
                case 2:
                case 3:
                    setSelectedStatus(prev => prev === num ? null : num)
                    setSelectedStartDate(null);
                    setSelectedEndDate(null);
                    break;
            }
        }, [selectedStatus, visible]
    )

    return (
        <RBSheet
            ref={sheetRef}
            openDuration={250}
            customStyles={{
                container: { ...styles.bottomSheetContainer, ...{ height: height } },
                draggableIcon: styles.draggableIcon
            }}
            closeOnDragDown={true}
            closeOnPressMask={true}
            onClose={closeSheet}
            keyboardAvoidingViewEnabled={true}
        >
            <View style={styles.container} onLayout={(e) => setHeight(e.nativeEvent.layout.height)}>
                <Text style={
                    styles.headerText
                }>{"Select filters"}</Text>
                <View style={styles.clearContainer}>
                    <Text style={
                        styles.headerLabel
                    }>{'Quick Date filters'}</Text>
                    <TouchableOpacity
                        onPress={onClearFilter}>
                        <Text
                            style={
                                styles.clearText
                            }
                        >{'Clear All'}</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.radioContainerVertical}>
                    <RadioButton
                        label={'Last Week'}
                        value={1}
                        isSelected={selectedStatus === 1}
                        onPress={() => onSelectFilterNum(1)}
                    />
                    <AddHeight size={20} />
                    <RadioButton
                        label={'Last 15 Days'}
                        value={2}
                        isSelected={selectedStatus === 2}
                        onPress={() => onSelectFilterNum(2)}
                    />
                    <AddHeight size={20} />
                    <RadioButton
                        label={'Last 1 Month'}
                        value={3}
                        isSelected={selectedStatus === 3}
                        onPress={() => onSelectFilterNum(3)}
                    />
                </View>
                <View style={styles.clearContainer}>
                    <Text style={
                        styles.headerLabel
                    }>{'Select Start & End Date'}</Text>
                </View>
                <TouchableOpacity
                    onPress={() => { selectedStatus != 1 && selectedStatus != 2 && selectedStatus != 3 && setShowCalendar(true) }}
                    style={styles.calendarButton}
                >
                    <CalendarIcon height={scale(27)} width={scale(27)} />
                    <View style={styles.pipe} />
                    <Text style={
                        [styles.normalLabel, styles.calendarLabel]
                    }>{`${getDateToShow(selectedStartDate)} - ${getDateToShow(selectedEndDate, true)}`}</Text>
                </TouchableOpacity>
                <AddHeight size={20} />
                <Button
                    onPress={onSort}
                    label={"SORT"}
                />
                <AddHeight size={25} />
            </View>
            <Calendar
                visible={showCalendar}
                allowRange={true}
                onClose={onCloseCalendar}
                onDateRangeSelected={onDateRangeSelected}
                startDate={selectedStartDate}
                endDate={selectedEndDate}
            />
        </RBSheet >
    )
}

FilterOrderHistory.defaultProps = {
    visible: false,
    startDate: null,
    endDate: null,
    status: null,
    onClose: () => { },
    onPress: (startDate, endDate, status) => { },
}

export default FilterOrderHistory;