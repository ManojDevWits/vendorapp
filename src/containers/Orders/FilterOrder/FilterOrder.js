import React, { useEffect, useState, useCallback, useRef } from 'react'
import { View, Text, ScrollView, TouchableOpacity, FlatList } from 'react-native'
import { Colors, FontFamily, fontScale, scale } from '../../../theme';
import { AddWidth, AddHeight } from '../../../components/AddSpace/AddSpace'
import styles from './FilterOrder.styles'
import CalendarIcon from '../../../assets/icons/ic_calendar.svg';
import SelectedIcon from '../../../assets/icons/ic_select.svg';
import UnselectedIcon from '../../../assets/icons/ic_select_grey.svg';
import BottomSheet from '../../../components/BottomSheet/BottomSheet';
import Button from '../../../components/Button/Button';
import { Constant } from '../../../constants/constant';
import Calendar from '../../../components/Calendar/Calendar';
import RBSheet from "react-native-raw-bottom-sheet";
import moment from 'moment';


const radioIconSize = scale(22);
const FilterOrder = ({
    visible,
    startDate,
    endDate,
    status,
    onClose,
    onPress
}) => {
    const sheetRef = useRef(null)
    const [selectedStartDate, setSelectedStartDate] = useState(startDate)
    const [selectedEndDate, setSelectedEndDate] = useState(endDate)
    const [selectedStatus, setSelectedStatus] = useState(status)
    const [showCalendar, setShowCalendar] = useState(false)

    const onCloseCalendar = useCallback(
        () => {
            setShowCalendar(false);
        },
        [showCalendar],
    )

    useEffect(() => {
        if (sheetRef) {
            if (visible) {
                sheetRef.current.open()
            } else {
                sheetRef.current.close()
            }
        } else {
            console.log("NO SHEET")
        }
    }, [visible])

    const getDateToShow = (date, isEndDate) => {
        if (date && date != null && date != "") {
            const dateType = moment(date, Constant.DateFormat.DD_MM_YYYY);
            return moment(dateType).format(Constant.DateFormat.DD_MMM_YYYY).toString();
        } else if (isEndDate) {
            return 'End Date'
        } else {
            return 'Start Date'
        }
    }

    const onDateRangeSelected = useCallback(
        (startDate, endDate) => {
            setSelectedStartDate(startDate)
            setSelectedEndDate(endDate)
            setShowCalendar(false)
        },
        [showCalendar],
    )

    const onSort = useCallback(
        () => {
            onPress(selectedStartDate, selectedEndDate, selectedStatus)
        },
        [selectedStartDate, selectedEndDate, selectedStatus, showCalendar],
    )

    const onClearFilter = useCallback(
        () => {
            setSelectedStartDate(null)
            setSelectedEndDate(null)
            setSelectedStatus(null)
        }
    )

    const RadioStatus = ({ label }) => {
        return (
            <TouchableOpacity
                onPress={() => { setSelectedStatus(prev => label) }}
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                {label === selectedStatus ? <SelectedIcon height={scale(radioIconSize)} width={scale(radioIconSize)} /> : <UnselectedIcon height={scale(radioIconSize)} width={scale(radioIconSize)} />}
                <AddWidth size={10} />
                <Text style={styles.normalLabel}>{label}</Text>
            </TouchableOpacity>
        )
    }

    return (
        // <BottomSheet
        //     isVisible={visible}
        //     onClose={onClose}
        //     container={{ height: scale(380) }}
        //     // modal={CalendarModal}
        // >
        <RBSheet
            ref={sheetRef}
            openDuration={250}
            customStyles={{
                // wrapper: { backgroundColor: 'red' },
                container: styles.bottomSheetContainer,
                draggableIcon: styles.draggableIcon
            }}
            closeOnDragDown={true}
            // closeOnPressMask={true}
            onClose={onClose}
            keyboardAvoidingViewEnabled={true}
        >
            <View style={styles.container}>
                <Text style={
                    styles.headerText
                }>{"Filter"}</Text>
                <View style={styles.clearContainer}>
                    <Text style={
                        styles.normalLabel
                    }>{'Select Start & End Date'}</Text>
                    <TouchableOpacity
                        onPress={onClearFilter}>
                        <Text
                            style={
                                styles.clearText
                            }
                        >{'Clear All'}</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity
                    onPress={() => { setShowCalendar(true) }}
                    style={styles.calendarButton}
                >
                    <CalendarIcon height={scale(27)} width={scale(27)} />
                    <View style={styles.pipe} />
                    <Text style={
                        [styles.normalLabel, styles.calendarLabel]
                    }>{`${getDateToShow(selectedStartDate)} - ${getDateToShow(selectedEndDate, true)}`}</Text>
                </TouchableOpacity>
                <AddHeight size={20} />
                <Text style={
                    styles.normalLabel
                }>{'Sort by Status'}</Text>
                <View style={styles.radioContainer}>
                    <View style={{
                        flex: 1,
                    }}>
                        <RadioStatus label={Constant.OrderStatus.PENDING} />
                        <AddHeight size={15} />
                        {/* <RadioStatus label={Constant.OrderStatus.ACKNOWLEDGED} /> */}
                    </View>
                    <View style={{
                        flex: 1,
                    }}>
                        <RadioStatus label={'Cancel'} />
                        <AddHeight size={15} />
                        {/* <RadioStatus label={Constant.OrderStatus.DELIVERED} /> */}
                    </View>
                </View>
                <AddHeight size={20} />
                <Button
                    onPress={onSort}
                    label={"SORT"}
                />
            </View>
            <Calendar
                visible={showCalendar}
                allowRange={true}
                onClose={onCloseCalendar}
                onDateRangeSelected={onDateRangeSelected}
                startDate={selectedStartDate}
                endDate={selectedEndDate}
            />
        </RBSheet >
    )
}

FilterOrder.defaultProps = {
    visible: false,
    startDate: null,
    endDate: null,
    status: null,
    onClose: () => { },
    onPress: (startDate, endDate, status) => { },
}

export default FilterOrder;