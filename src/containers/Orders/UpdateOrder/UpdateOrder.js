import React, { useRef, useEffect, useState, useCallback } from 'react'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { View, Text, ScrollView, TouchableOpacity, StyleSheet } from 'react-native'
import { showToast } from '../../../utils/Toast';
import StatusBar from '../../../components/StatusBar/StatusBar';
import { Colors, FontFamily, fontScale, scale } from '../../../theme';
import { AddWidth, AddHeight } from '../../../components/AddSpace/AddSpace'
import styles from './UpdateOrder.styles'
import CalendarIcon from '../../../assets/icons/ic_calendar.svg';
import CalendarDisabledIcon from '../../../assets/icons/ic_calendar_disabled.svg';
import TimeIcon from '../../../assets/icons/ic_time_maroon.svg';
import TimeDisabledIcon from '../../../assets/icons/ic_time_disabled.svg';
import SelectedIcon from '../../../assets/icons/ic_select.svg';
import UnselectedIcon from '../../../assets/icons/ic_select_grey.svg';
import DelayedOrdersIcon from '../../../assets/images/img_clock.svg';
import CanceledOrdersIcon from '../../../assets/images/img_cancel_order.svg';
import PageLabel from '../../../components/PageLabel/PageLabel';
import InfoLabel from '../../../components/InfoLabel/InfoLabel';
import OrderItem from '../../../components/OrderItem/OrderItem';
import ROUTES from '../../../navigators/Routes';
import SearchFilter from '../../../components/SearchFilter/SearchFilter';
import BottomSheet from '../../../components/BottomSheet/BottomSheet';
import Button from '../../../components/Button/Button';
import { Constant } from '../../../constants/constant';
import RBSheet from 'react-native-raw-bottom-sheet';
import moment from 'moment';
import Calendar from '../../../components/Calendar/Calendar';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import RadioButton from '../../../components/RadioButton/RadioButton';
import TimePicker from '../../../components/TimePicker/TimePicker';
import DateTimePicker from '@react-native-community/datetimepicker';


const verticalGap = 25;
const calendarIconSize = scale(22);
const timerIconSize = scale(25);
const UpdateOrder = ({
    item,
    index,
    visible,
    estDate,
    estTime,
    ackDate,
    ackTime,
    pickDate,
    pickTime,
    onClose,
    onPress,
    updateAck,
    updatePickup
}) => {
    const sheetRef = useRef(null)

    //not in use start
    // const [estimatedDate, setEstimatedDate] = useState(estDate)
    // const [estimatedTime, setEstimatedTime] = useState(estTime)
    // const [acknowledgementDate, setAcknowledgementDate] = useState(ackDate)
    // const [acknowledgementTime, setAcknowledgementTime] = useState(ackTime)
    // const [showEstCalendar, setShowEstCalendar] = useState(false)
    // const [showAckCalendar, setShowAckCalendar] = useState(false)
    // const [showEstTimePicker, setShowEstTimePicker] = useState(false)
    // const [showAckTimePicker, setShowAckTimePicker] = useState(false)
    //not in use end
    const [height, setHeight] = useState(scale(460));
    const [pickupDate, setPickupDate] = useState(null)
    const [pickupTime, setPickupTime] = useState(null)
    const [showPickCalendar, setShowPickCalendar] = useState(false)
    const [showPickTimePicker, setShowPickTimePicker] = useState(false)
    const [selectedStatus, setSelectedStatus] = useState(false)

    // useEffect(() => {
    //     if(item){
    //         console.log("ITEM_TO_UPDATE:"+JSON.stringify(item))
    //     }else{
    //         console.log("NO_ITEM_TO_UPDATE")
    //     }

    // }, [item])

    useEffect(() => {
        if (sheetRef) {
            if (visible) {
                sheetRef.current.open()
            } else {
                sheetRef.current.close()
            }
        }
    }, [visible])

    const onCloseTimePicker = useCallback(
        () => {
            // setShowEstTimePicker(false);
            // setShowAckTimePicker(false);
            setShowPickTimePicker(false);
        },
        [showPickTimePicker],
    )

    const onPickupTimeSelected = useCallback(
        (time) => {
            console.log("Time" + time)
            setPickupTime(time)
            setShowPickTimePicker(false);
        },
        [showPickTimePicker],
    )

    const onCloseCalendar = useCallback(
        () => {
            // setShowEstCalendar(false);
            // setShowAckCalendar(false);
            setShowPickCalendar(false);
        },
        [showPickCalendar],
    )

    // const onEstDateSelected = useCallback(
    //     (date) => {
    //         setEstimatedDate(date)
    //         setShowEstCalendar(false);
    //     },
    //     [showEstCalendar],
    // )

    // const onAckDateSelected = useCallback(
    //     (date) => {
    //         setAcknowledgementDate(date)
    //         setShowAckCalendar(false);
    //     },
    //     [showAckCalendar],
    // )

    const onPickDateSelected = useCallback(
        (date) => {
            setPickupDate(date)
            setShowPickCalendar(false);
        },
        [showPickCalendar],
    )

    const onUpdateOrder = useCallback(
        () => {
            if (item && item.orderStatus) {
                if (item.orderStatus == Constant.OrderStatus.PENDING_ACK) {
                    updateAck(item, index, orderStatus)
                } else if (item.orderStatus == Constant.OrderStatus.PENDING_PICKUP) {
                    if ((pickupDate == null && pickupTime != null)
                        || (pickupDate != null && pickupTime == null)) {
                        showToast("Please select pickup date and time");
                        return;
                    }
                    updatePickup(item, index, pickupDate, pickupTime)
                }
            }
            // onPress(item, index, selectedStatus, pickupDate, pickupTime);
        },
        [pickupDate, pickupTime, showPickCalendar, item],
    )

    const getDateToShow = (date) => {
        if (date && date != null && date != "") {
            const dateType = moment(date, Constant.DateFormat.DD_MM_YYYY);
            return moment(dateType).format(Constant.DateFormat.DD_MMM_YYYY).toString();
        } else {
            return 'Select Date'
        }
    }

    const getDateToShowFromMillis = (date) => {
        if (date && date != null && date != "" && !isNaN(date)) {
            return moment(date * 1000, "x").format(Constant.DateFormat.DD_MMM_YYYY).toString();
        } else {
            return 'Invalid Date'
        }
    }

    const getTimeToShowFromMillis = (date) => {
        if (date && date != null && date != "" && !isNaN(date)) {
            return moment(date * 1000, "x").format(Constant.DateFormat.HH_MM_A).toString();
        } else {
            return 'Invalid Time'
        }
    }

    const DateView = ({ data, isDisabled, onPress = () => { } }) => {
        return (
            <TouchableOpacity style={[styles.calendarButton, isDisabled ? styles.disableButtonColor : {}]} onPress={isDisabled ? () => { } : onPress}>
                {
                    isDisabled ? <CalendarDisabledIcon height={calendarIconSize} width={calendarIconSize} />
                        : <CalendarIcon height={calendarIconSize} width={calendarIconSize} />
                }
                <View style={isDisabled ? styles.disablePipe : styles.pipe} />
                <Text style={
                    [styles.normalLabel, isDisabled ? styles.disabledCalendarLabel : styles.calendarLabel]
                }>{data}</Text>
            </TouchableOpacity>
        )
    }

    const TimeView = ({ data, isDisabled, onPress = () => { } }) => {
        return (
            <TouchableOpacity style={[styles.calendarButton, isDisabled ? styles.disableButtonColor : {}]} onPress={isDisabled ? () => { } : onPress}>
                {
                    isDisabled ? <TimeDisabledIcon height={timerIconSize} width={timerIconSize} />
                        : <TimeIcon height={timerIconSize} width={timerIconSize} />
                }
                <View style={isDisabled ? styles.disablePipe : styles.pipe} />
                <Text style={
                    [styles.normalLabel, isDisabled ? styles.disabledCalendarLabel : styles.calendarLabel]
                }>{data}</Text>
            </TouchableOpacity>
        )
    }

    console.log("SIZE"+height)
    return (
        <RBSheet
            ref={sheetRef}
            openDuration={250}
            customStyles={{
                container: {...styles.bottomSheetContainer,...{ height: height }},
                draggableIcon: styles.draggableIcon
            }}
            closeOnDragDown={true}
            onClose={onClose}
            keyboardAvoidingViewEnabled={true}
        >
            <View style={styles.container} onLayout={(e) => setHeight(e.nativeEvent.layout.height)}>
                <Text style={
                    styles.headerText
                }>{"Update Orders"}</Text>
                <AddHeight size={verticalGap} />
                <Text style={
                    styles.headerLabel
                }>{'Estimated Date & Time'}</Text>
                <View style={styles.dateTimeContainer}>
                    <DateView data={"06 Jun 2021"} isDisabled={true} onPress={() => { }} />
                    <TimeView data={'04 : 40 AM'} isDisabled={true} onPress={() => { }} />
                </View>
                <AddHeight size={verticalGap} />
                <Text style={
                    styles.headerLabel
                }>{item && item.orderStatus == Constant.OrderStatus.PENDING_ACK ? 'Acknowledgement Confirmation' : 'Acknowledgement Date & Time'}</Text>

                {
                    item && item.orderStatus == Constant.OrderStatus.PENDING_ACK
                        ? <RadioButton
                            containerStyle={{ marginTop: scale(15) }}
                            label={'Acknowledgement'}
                            value={Constant.OrderStatus.PENDING_ACK}
                            isSelected={selectedStatus === Constant.OrderStatus.PENDING_ACK}
                            onPress={() => { setSelectedStatus(prev => prev === Constant.OrderStatus.PENDING_ACK ? null : Constant.OrderStatus.PENDING_ACK) }}
                        />
                        : <View style={styles.dateTimeContainer}>
                            <DateView data={getDateToShowFromMillis(item?.acknowledgment_date_time)} isDisabled={true} onPress={() => { }} />
                            <TimeView data={getTimeToShowFromMillis(item?.acknowledgment_date_time)} isDisabled={true} onPress={() => { }} />
                        </View>
                }
                <AddHeight size={verticalGap} />
                <Text style={
                    styles.headerLabel
                }>{'Actual Pickup Date & Time'}</Text>
                <View style={styles.dateTimeContainer}>
                    <DateView data={getDateToShow(pickupDate)} onPress={() => { item && item.orderStatus == Constant.OrderStatus.PENDING_PICKUP ? setShowPickCalendar(true) : null }} />
                    <TimeView data={pickupTime?pickupTime:'Select Time'} onPress={() => { item && item.orderStatus == Constant.OrderStatus.PENDING_PICKUP ? setShowPickTimePicker(true) : null }} />
                </View>
                <AddHeight size={verticalGap} />
                <Button
                    onPress={onUpdateOrder}
                    label={"UPDATE ORDERS"}
                />
                <AddHeight size={verticalGap} />
            </View>
            {/* <Calendar
                visible={showEstCalendar}
                allowRange={false}
                onClose={onCloseCalendar}
                onDateSelected={onEstDateSelected}
                date={estimatedDate}
            />
            <Calendar
                visible={showAckCalendar}
                allowRange={false}
                onClose={onCloseCalendar}
                onDateSelected={onAckDateSelected}
                date={acknowledgementDate}
            /> */}
            <Calendar
                visible={showPickCalendar}
                allowRange={false}
                onClose={onCloseCalendar}
                onDateSelected={onPickDateSelected}
                date={pickupDate}
            />
            <TimePicker
                visible={showPickTimePicker}
                onClose={onCloseTimePicker}
                value={pickupTime}
                onTimeSelected={onPickupTimeSelected}
            />
        </RBSheet>
    )
}

UpdateOrder.defaultProps = {
    visible: false,
    estDate: null,
    estTime: null,
    ackDate: null,
    ackTime: null,
    pickDate: null,
    pickTime: null,
    onClose: () => { },
    onPress: () => { }
}
export default UpdateOrder;