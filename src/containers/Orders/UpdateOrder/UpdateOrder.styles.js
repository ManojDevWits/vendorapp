import { StyleSheet } from "react-native"
import { Colors, fontScale, FontFamily, scale, globalStyle } from "../../../theme";

const shadow = {
    backgroundColor: Colors.white,
    borderRadius: scale(5),
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 0,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4,
    elevation: 2,
}
const styles = StyleSheet.create({
    bottomSheetContainer: {
        borderTopLeftRadius: scale(20),
        borderTopRightRadius: scale(20),
        height: scale(460),
    },
    draggableIcon: {
        backgroundColor: Colors.draggableIcon,
        width: scale(100),
        height: scale(4)
    },
    container: {
        paddingTop: scale(10),
        paddingBottom: scale(20),
        paddingHorizontal: scale(20),
    },
    headerText: {
        alignSelf: 'center',
        fontFamily: FontFamily.Bold,
        color: Colors.black,
        fontSize: fontScale(19)
    },
    dateTimeContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    clearText: {
        color: Colors.clearAll,
        textDecorationLine: 'underline',
        fontFamily: FontFamily.Medium,
        fontSize: fontScale(15),
    },
    pipe: {
        width: scale(1),
        height: scale(20),
        backgroundColor: Colors.primary,
        marginHorizontal: scale(12)
    },
    disablePipe: {
        width: scale(1),
        height: scale(20),
        backgroundColor: Colors.mortar,
        marginHorizontal: scale(12)
    },
    calendarButton: {
        ...shadow,
        height: scale(40),
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: scale(12),
        paddingVertical: scale(6),
        marginTop: scale(10),
    },
    disableButtonColor:{
        backgroundColor:Colors.whiteSmoke
    },
    headerLabel:{
        color: Colors.black,
        fontFamily: FontFamily.Medium,
        fontSize: fontScale(16),
    },
    normalLabel: {
        color: Colors.black,
        fontFamily: FontFamily.Regular,
        fontSize: fontScale(15),
    },
    calendarLabel: {
        color: Colors.primary,
    },
    disabledCalendarLabel: {
        color: Colors.mortar,
    },
    radioContainer: {
        flexDirection: 'row',
        marginTop: scale(10)
    },
})

export default styles;