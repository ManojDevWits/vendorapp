import { StyleSheet } from "react-native"
import { Colors, fontScale, FontFamily, scale , globalStyle} from "../../theme";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    innerContainer: {
        flex: 1,
        flexDirection: "column",
        padding: 20,
    },
    banner: {
        alignItems: 'center',
    },
    backIcon: {
        position: 'absolute',
        top: scale(20),
        left: scale(20),
    },
    timer: {
        color: Colors.black,
        fontFamily: FontFamily.Regular,
        fontSize: fontScale(16),
    },
    resendText: {
        color: Colors.primary,
        textDecorationLine: 'underline',
        fontFamily: FontFamily.Medium,
        fontSize: fontScale(18),
    },
    resendDisableText:{
        opacity:0.3
    },
    otpInput:{
        ...globalStyle.defaultInputStyle,
        width: scale(66),
        borderWidth:0,
        borderBottomColor: Colors.black,
        borderBottomWidth: scale(2.6),
    },
    otpViewContainer:{ 
        marginHorizontal: scale(20) 
    },
    otpView:{ 
        width: '100%', 
        height: scale(70), 
    },
    timerContainer:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: scale(20),
        marginTop: scale(14),
    },
    timerInnerContainer:{
        flexDirection: 'row',
    }
})

export default styles;