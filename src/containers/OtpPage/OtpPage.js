import React, { useEffect, useCallback, useState, useRef } from 'react'
import { View, Text, Image, TextInput, TouchableOpacity, ScrollView, StyleSheet } from 'react-native'
import SplashScreen from 'react-native-splash-screen'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from './OtpPage.styles'
import { useDispatch } from "react-redux"
import OtpBanner from '../../assets/images/img_verify_number.svg'
import PageLabel from '../../components/PageLabel/PageLabel'
import InfoLabel from '../../components/InfoLabel/InfoLabel'
import Button from '../../components/Button/Button'
import { Colors, scale } from '../../theme'
import { AddHeight, AddWidth } from '../../components/AddSpace/AddSpace';
import { showToast } from '../../utils/Toast';
import BackIcon from '../../assets/icons/ic_back.svg'
import TimerIcon from '../../assets/icons/ic_chronometer.svg'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import StatusBar from '../../components/StatusBar/StatusBar';
import NavigationService from '../../service/NavigationService';
import { callApiSaga, updateLoginSessionAction, updateTokenAction, updateUserDataAction, updateSplashAction } from '../../redux/actions';
import ROUTES from '../../navigators/Routes';
import { Constant } from '../../constants';

const OtpPage = ({ route, navigation }) => {
    const { mobile } = route.params;
    const dispatch = useDispatch();
    const [otp, setOtp] = useState(undefined);
    const [timer, setTimer] = useState(30);

    useEffect(() => {
        SplashScreen.hide()
    }, [])

    useEffect(() => {
        const count = setInterval(() => {
            if (timer > 0) {
                setTimer(prev => prev - 1);
            }
        }, 1000);
        // clearing interval
        return () => clearInterval(count);
    }, [timer])

    const onResend = useCallback(
        () => {
            if (timer === 0) {
                // setTimer(prev => 30)
                let formData = new FormData();
                formData.append('mobile', mobile)
                dispatch(callApiSaga({
                    params: null,
                    request: formData,
                    screen: ROUTES.OTP_PAGE,
                    apiType: Constant.ApiType.RESEND_OTP,
                    successCallback: (res) => {
                        setTimer(prev => 30)
                    },
                    failureCallback: (res) => {

                    }
                }))
            }
        },
        [timer],
    )

    const onLogin = useCallback(
        () => {
            if (!otp || otp.length < 4) {
                showToast("Please enter valid otp");

            } else {
                // dispatch(updateLoginSessionAction(true))
                // dispatch(updateTokenAction("c4442ebe6bfb185fb27fda86a1cd18a5@OTAzNDg3NTA1MQ=="))
                // dispatch(updateUserDataAction({ 'name': 'Manoj Saini' }))
                // return;
                let formData = new FormData();
                formData.append('mobile', mobile);
                formData.append('otp', otp);
                dispatch(callApiSaga({
                    params: null,
                    request: formData,
                    screen: ROUTES.OTP_PAGE,
                    apiType: Constant.ApiType.VERIFY_MOBILE,
                    successCallback: (res) => {
                        //save user data, token and then navigate
                        console.log("OTP_RES:" + JSON.stringify(res))
                        // NavigationService.navigate(ROUTES.DASHBOARD_PAGE)
                        if (res && res.data && res.data.token && res.data.name) {
                            //It will navigate to dashboard when session is true
                            dispatch(updateLoginSessionAction(true))
                            dispatch(updateTokenAction(res.data.token))
                            dispatch(updateUserDataAction({ 'name': res.data.name }))
                        } else {
                            showToast('Token or name is missing')
                        }
                    },
                    failureCallback: (res) => { }
                }))
            }
        },
        [otp],
    )

    return (
        <View style={styles.container}>
            <StatusBar />
            <KeyboardAwareScrollView
                contentContainerStyle={{ flex: 1 }}>
                <View style={
                    styles.innerContainer
                }>
                    <AddHeight size={20} />
                    <View style={styles.banner}>
                        <OtpBanner width={scale(234)} height={scale(186)} />
                    </View>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={
                            styles.backIcon
                        }
                        onPress={() => { NavigationService.goBack() }}
                    >
                        <BackIcon />
                    </TouchableOpacity>
                    <AddHeight size={40} />
                    <PageLabel label={"Verify Your Mobile Number"} />
                    <AddHeight size={20} />
                    <InfoLabel label={`Verification Code Sent to ${mobile}`} />

                    <AddHeight size={20} />
                    <View style={styles.otpViewContainer}>
                        <OTPInputView
                            style={styles.otpView}
                            pinCount={4}
                            // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                            onCodeChanged={code => { setOtp(code) }}
                            autoFocusOnLoad
                            codeInputFieldStyle={styles.otpInput}
                            codeInputHighlightStyle={styles.otpInput}
                            selectionColor={Colors.primary}
                            onCodeFilled={(code => {
                                console.log(`Code is ${code}, you are good to go!`)
                            })}
                        />
                    </View>

                    <View style={
                        styles.timerContainer
                    }>
                        <View
                            style={
                                styles.timerInnerContainer
                            }
                        >
                            <TimerIcon height={scale(22)} width={scale(22)} />
                            {/* <Rupee /> */}
                            {/* <Search /> */}
                            {/* <Time /> */}
                            <AddWidth size={5}/>
                            <Text style={
                                styles.timer
                            }>
                                {
                                    "00:" + (timer === 0
                                        ? "00"
                                        : timer < 10
                                            ? "0" + timer
                                            : timer)
                                }
                            </Text>
                        </View>
                        <TouchableOpacity
                            onPress={onResend}
                            disabled={timer > 0}>
                            <Text
                                style={
                                    [styles.resendText, timer > 0 ? styles.resendDisableText : {}]
                                }
                            >{'Resend'}</Text>
                        </TouchableOpacity>

                    </View>
                    <AddHeight size={36} />
                    <Button onPress={onLogin} label={"LOGIN"} />
                    <AddHeight size={20} />
                </View>
            </KeyboardAwareScrollView>
        </View >
    )
}

export default OtpPage;