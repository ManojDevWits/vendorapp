import { StyleSheet } from "react-native"
import { Colors, fontScale, FontFamily, scale, globalStyle } from "../../theme";

const shadow = {
    backgroundColor: Colors.white,
    borderRadius: scale(10),
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 0,
    },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 3,
    borderWidth: 2.5,
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    innerContainer: {
        flex: 1,
        flexDirection: "column",
        padding: 20,
    },
    card: {
        ...shadow,
        backgroundColor: Colors.whiteSmoke,
        borderColor: Colors.whiteSmoke2,
        borderWidth: 0.5,
    },
    section: {
        padding: scale(10),
    },
    image:{ 
        height: scale(100), 
        resizeMode: 'cover', 
        borderRadius: 5,
        backgroundColor:Colors.gainsboro 
    },
    divider:{ 
        backgroundColor: Colors.whisper, 
        height: 1.5 
    },
    label:{
        fontFamily: FontFamily.Bold,
        color: Colors.black,
        fontSize: fontScale(17),
        alignSelf: 'center'
    },
    buttonStyle:{
        backgroundColor: Colors.white,
        borderRadius: 50,
        borderColor: Colors.whiteSmoke2,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText:{
        fontFamily: FontFamily.Regular,
        color: Colors.primary,
        fontSize: fontScale(16),
        margin: scale(2),
        padding:scale(4)
    }
})

export default styles;