import React, { useEffect } from 'react'
import SplashScreen from 'react-native-splash-screen'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { View, Text, ScrollView, TouchableOpacity, Image } from 'react-native'
import { showToast } from '../../utils/Toast';
import StatusBar from '../../components/StatusBar/StatusBar';
import Header from '../../components/Header/Header';
import { Colors, FontFamily, fontScale, scale } from '../../theme';
import { AddWidth, AddHeight } from '../../components/AddSpace/AddSpace'
import styles from './ComplianceDocumentPage.styles'
import PlusIcon from '../../assets/icons/ic_plus.svg';
import PendingOrdersIcon from '../../assets/images/img_time.svg';
import DelayedOrdersIcon from '../../assets/images/img_clock.svg';
import CanceledOrdersIcon from '../../assets/images/img_cancel_order.svg';
import PageLabel from '../../components/PageLabel/PageLabel';
import InfoLabel from '../../components/InfoLabel/InfoLabel';

const imageUrl = 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png'
const ComplianceDocumentPage = () => {
    useEffect(() => {
        SplashScreen.hide()
    }, [])


    const renderRecord = (title, url, onPress) => {
        return (
            <View>
                <View style={styles.card}>
                    <View style={styles.section}>
                        {
                            url ?
                                <Image
                                    source={{ uri: url }}
                                    style={styles.image}
                                />
                                :
                                <TouchableOpacity style={styles.image}>
                                    <PlusIcon height={scale(100)} width={'100%'} />
                                </TouchableOpacity>

                        }

                    </View>
                    <View style={styles.divider} />
                    <View style={styles.section}>
                        <Text
                            style={styles.label}>{title}</Text>
                        <AddHeight size={5} />
                        <TouchableOpacity
                            onPress={onPress}
                            style={styles.buttonStyle}>
                            <Text style={styles.buttonText}>{url?'Download':'Upload'}</Text>
                        </TouchableOpacity>
                        <AddHeight size={5} />
                    </View>
                </View>
                <AddHeight size={30} />
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <StatusBar color={Colors.statusbar} />
            <Header />
            <ScrollView
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={{ flexGrow: 1 }}>
                <View style={
                    styles.innerContainer
                }>
                    <PageLabel label={"Vendor Collaterals and Docs"} />
                    <AddHeight size={20} />
                    {renderRecord("Completed Orders", null, () => { })}
                    {renderRecord("Pending Orders", imageUrl, () => { })}
                    {renderRecord("Delayed Orders", imageUrl, () => { })}
                </View>
            </ScrollView>
        </View >
    )
}

export default ComplianceDocumentPage;