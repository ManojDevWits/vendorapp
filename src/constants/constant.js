export const Constant = {
    RequestType: {
        GET: 'GET',
        POST: 'POST',
        PUT: 'PUT',
        DELETE: 'DELETE',
    },
    ContentType: {
        JSON: "application/json",
        MULTIPART: "multipart/form-data",
    },
    OrderStatus: {
        UPDATED: 'Updated',
        COMPLETE: 'Complete',
        CANCELLED: 'Cancelled',
        PENDING: 'Pending',
        PAID: 'Paid',
        CONFIRMED: 'Confirmed',
        PENDING_PICKUP: 'Pending Pickup',
        PENDING_ACK: 'Pending Acknowledgement',
        DELAYED: 'Delayed',
    },
    TimeType: {
        ESTIMATED: 'Estimated',
        ACKNOWLEDGEMENT: 'Acknowledgement',
        PICKUP: 'Pick up',
    },
    DateFormat: {
        DD_MM_YYYY: 'DD-MM-YYYY',
        DD_MMM_YYYY: 'DD MMM YYYY',
        HH_MM_A: 'hh : mm A',
        DD_MM_YYYY_HH_MM_A: 'DD-MM-YYYY hh : mm A',
        DD_MMM_YYYY_HH_MM: 'DD MMM YYYY, HH:MM',
    },
    ApiType: {
        VERIFY_TOKEN: "VERIFY_TOKEN",
        VERIFY_MOBILE: "VERIFY_MOBILE",
        SEND_OTP: "SEND_OTP",
        RESEND_OTP: "RESEND_OTP",
        DASHBOARD: "DASHBOARD",
        MY_ORDERS: "MY_ORDERS",
        HISTORY_ORDERS: "HISTORY_ORDERS",
        ORDER_DELAYED: "ORDER_DELAYED",
        UPDATE_ACK_DATE_TIME: "UPDATE_ACK_DATE_TIME",
        UPDATE_PICKUP_DATE_TIME: "UPDATE_PICKUP_DATE_TIME",
    },
    defaultFilterData : { 
        startDate: null, 
        endDate: null, 
        status: null, 
        searchText: '' 
    }

}
