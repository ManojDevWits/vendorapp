import * as React from 'react';

export const navigationRef = React.createRef();

const navigate = (name, params) => {
    navigationRef.current?.navigate(name, params);
}

const navigateChild = (name, params) => {
    navigationRef.current?.navigate(name, params);
}

const goBack = () => {
    navigationRef.current?.goBack();
}

const openDrawer = () => {
    navigationRef.current?.openDrawer();
}

const closeDrawer = () => {
    navigationRef.closeDrawer();
}

export default { navigate, goBack, openDrawer,closeDrawer }

