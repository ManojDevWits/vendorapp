import { Dimensions, Platform, PixelRatio } from "react-native";

export const { width, height } = Dimensions.get("screen");

console.log("WIDTH:" + width);
console.log("HEIGHT:" + height);
const deviceSize = (width + height) / 2;

const guidelineBaseWidth = 390;
const guidelineBaseHeight = 844;
const guidelineBaseSize = (guidelineBaseWidth + guidelineBaseHeight) / 2

export const scale = (size) => (deviceSize / guidelineBaseSize) * size;
export const scaleWidth = (size) => (width / guidelineBaseWidth) * size;
export const scaleHeight = (size) => (height / guidelineBaseHeight) * size;


export const fontScale = (size, factor = 0.5) => size + (scale(size) - size) * factor;
