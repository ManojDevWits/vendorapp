import { StyleSheet } from "react-native"
import { Colors } from "./colors";
import { fontScale, scale } from "./scale";
import { FontFamily } from "./fonts";


export const globalStyle = StyleSheet.create({
    defaultInputStyle:{
         height: scale(76),
         borderBottomColor: Colors.black,
         borderBottomWidth: scale(2.5),
         // paddingBottom: scale(4),
         fontSize: fontScale(26),
         // lineHeight: 36,
         letterSpacing: 2,
         fontFamily: FontFamily.Medium,
         color:Colors.black
    },
})
