import { baseUrl, config } from './config'
import axios from 'axios';
import { Constant } from '../constants';
import { store } from '../store'

class Api {
    static async baseHeaders() {
        const state = store.getState();
        console.log("STATE::::")
        console.log(JSON.stringify(state))
        const authToken = state.dataReducer.token;
        const isSessionAvailable = state.commonReducer.isSessionAvailable;
        if (authToken && isSessionAvailable) {
            return {
                "Content-Type": Constant.ContentType.MULTIPART,
                "Accept": Constant.ContentType.JSON,
                "token": authToken
            }
        } else {
            return {
                "Content-Type": Constant.ContentType.MULTIPART,
                "Accept": Constant.ContentType.JSON,
            }
        }

    }

    static get(endpoint, headers, params, timeout) {
        return this.api(Constant.RequestType.GET, endpoint, headers, params, null, timeout);
    }

    static post(endpoint, headers, params, data, timeout) {
        return this.api(Constant.RequestType.POST, endpoint, headers, params, data, timeout);
    }

    static put(endpoint, headers, params, data, timeout) {
        return this.api(Constant.RequestType.PUT, endpoint, headers, params, data, timeout);
    }

    static async api(requestType, endpoint, headers, params, body, timeout = 60000) {
        const url = `${baseUrl}${endpoint}`;
        // const url = `${endpoint}`;
        const baseHeaders = await Api.baseHeaders();
        const requestConfig = {
            headers: headers ? { ...baseHeaders, ...headers } : baseHeaders,
        }

        if (params) {
            requestConfig.params = params
        }

        // const bodySample = {
        //     method: requestType,
        //     url: url,
        //     // data: requestConfig.headers["Content-Type"] == Constant.ContentType.MULTIPART ? body : (requestType != Constant.RequestType.GET && body) ? JSON.stringify(body) : null,
        //     data: requestType != Constant.RequestType.GET && body ? body : null,
        //     headers: requestConfig.headers,
        //     // timeout:timeout,
        // }

        // console.log("BODY_SAMPLE:")
        // console.log(bodySample)

        return axios({
            method: requestType,
            url: url,
            // data: requestConfig.headers["Content-Type"] == Constant.ContentType.MULTIPART ? body : (requestType != Constant.RequestType.GET && body) ? JSON.stringify(body) : null,
            data: requestType != Constant.RequestType.GET && body ? body : null,
            headers: requestConfig.headers,
            timeout: timeout,
        }).then(res => {
            console.log("-----------AXIOS  Api Response is----------- ");
            console.log("url string " + url);
            console.log("header " + JSON.stringify(requestConfig.headers));
            console.log("body " + JSON.stringify(body));
            console.log("requestType " + requestType)
            // return res;
            return checkValidJson(res);
        }).catch(e => {
            console.log("url string " + url);
            console.log("header " + JSON.stringify(requestConfig.headers));
            console.log("body " + JSON.stringify(body));
            console.log("requestType " + requestType)
            console.log("-----------AXIOS  Api catch is-----------")
            console.log(e)
            console.log("catch Error" + JSON.stringify(e))
            throw e;
        })

    }

}

export default Api;

function checkValidJson(response) {
    if (response.data !== "string") return response;
    return Error(response);
}

