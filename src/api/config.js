export const baseUrl = "http://dev.justmyroots.com"
const api = "/api/Vendorapi/"
export const config = {
    endpoint:{
        verifyToken:`${api}verifyToken`,
        sendOtp:`${api}sentotp`,
        resendOtp:`${api}resentotp`,
        verifyMobile:`${api}verifyMobile`,
        dashboard:`${api}myDashboard`,
        myOrders:`${api}myOrders`,
        historyOrders:`${api}historyOrders`,
        orderDelayed:`${api}orderDelayed`,
        updateAckDateTime:`${api}updateAcknowledgmentDateTime`,
        updatePickupDateTime:`${api}updateActualPickupDateTime`,
    }
}