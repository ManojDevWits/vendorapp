import React from 'react'
import { TouchableOpacity, Text } from 'react-native'
import { Colors, scale } from '../../theme';
import styles from './RadioButton.styles';
import SelectedIcon from '../../assets/icons/ic_select.svg';
import UnselectedIcon from '../../assets/icons/ic_select_grey.svg';
import { AddWidth } from '../AddSpace/AddSpace';

const radioIconSize = scale(22);
const RadioButton = ({
    containerStyle = { },
    textStyle = {},
    label = "",
    value = "",
    isSelected = false,
    disable = false,
    isUnselectable = true,
    onPress = () => { } }) => {
    return (
        <TouchableOpacity
            onPress={isUnselectable ? onPress : () => { }}
            style={[styles.touchable, containerStyle]}>
            {isSelected ?
                <SelectedIcon height={scale(radioIconSize)} width={scale(radioIconSize)} />
                : <UnselectedIcon height={scale(radioIconSize)} width={scale(radioIconSize)} />}
            <AddWidth size={10} />
            <Text style={[styles.label, textStyle]}>{label}</Text>
        </TouchableOpacity>
    )
}

export default RadioButton;