import { StyleSheet } from "react-native"
import { Colors, FontFamily, fontScale, scale } from "../../theme";


const styles = StyleSheet.create({
    touchable:{
        flexDirection: 'row',
        alignItems: 'center',
    },
    label: {
        color: Colors.black,
        fontFamily: FontFamily.Regular,
        fontSize: fontScale(15),
    },
})

export default styles;