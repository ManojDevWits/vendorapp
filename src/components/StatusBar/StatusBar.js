import React from 'react'
import { View, SafeAreaView, StatusBar as Status } from 'react-native'
import { scale, Colors } from '../../theme'

export default StatusBar = ({ color = Colors.white }) => {
    return (
        <View >
            <SafeAreaView style={{ backgroundColor: color }} />
            <Status barStyle="dark-content" backgroundColor={color} />
        </View>
    )
}
