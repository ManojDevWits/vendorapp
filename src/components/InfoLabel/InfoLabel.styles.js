import { StyleSheet } from "react-native"
import { Colors, FontFamily, fontScale, scale } from "../../theme";


const styles = StyleSheet.create({
    label: {
        fontSize:fontScale(18),
        fontFamily:FontFamily.Regular,
        color:Colors.infoLabel
    },
})

export default styles;