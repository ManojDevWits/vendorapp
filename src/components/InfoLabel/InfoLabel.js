import React from 'react'
import { View, Text, Image } from 'react-native'
import { Colors } from '../../theme';
import styles from './InfoLabel.styles';

const InfoLabel = ({ label = "" }) => {
    return (
        <View>
            <Text style={
                styles.label
            }>{label}</Text>
        </View>
    )
}

export default InfoLabel;