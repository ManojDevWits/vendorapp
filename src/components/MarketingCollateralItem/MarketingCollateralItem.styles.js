import { StyleSheet } from "react-native"
import { Colors, FontFamily, fontScale, scale } from "../../theme";

const shadow = {
    backgroundColor: Colors.white,
    borderRadius: scale(5),
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4,
    elevation: 3,
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.whiteSmoke
    },
    card: {
        ...shadow,
        backgroundColor: Colors.whiteSmoke,
        borderColor: Colors.whiteSmoke2,
        borderWidth: 0.5,
        width:'90%'
    },
    section: {
        padding: scale(10),
    },
    image:{ 
        height: scale(100), 
        resizeMode: 'cover', 
        borderRadius: 5 
    },
    divider:{ 
        backgroundColor: Colors.whisper, 
        height: 1.5 
    },
    label:{
        fontFamily: FontFamily.Bold,
        color: Colors.black,
        fontSize: fontScale(17),
        alignSelf: 'center'
    },
    buttonStyle:{
        backgroundColor: Colors.white,
        borderRadius: 50,
        borderColor: Colors.whiteSmoke2,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText:{
        fontFamily: FontFamily.Regular,
        color: Colors.primary,
        fontSize: fontScale(16),
        margin: scale(2),
    }
})

export default styles;