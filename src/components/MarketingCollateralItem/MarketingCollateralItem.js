import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { Colors, FontFamily, fontScale, scale } from '../../theme';
import styles from './MarketingCollateralItem.styles';
import ROUTES from '../../navigators/Routes';
import { AddHeight } from '../AddSpace/AddSpace';

const MarketingCollateralItem = ({ item, index, screen, onPress }) => {
    return (
        <View style={{
            flex: 1,
            flexDirection: "row",
            marginBottom:scale(30),
            justifyContent:index%2 === 0 ? 'flex-start':'flex-end'
        }}>
            <View style={styles.card}>
                <View style={styles.section}>
                    <Image
                        source={{ uri: item.url }}
                        style={styles.image}
                    />
                </View>
                <View style={styles.divider} />
                <View style={styles.section}>
                    <Text
                        style={styles.label}>{item?.title}</Text>
                    <AddHeight size={5} />
                    <TouchableOpacity
                        onPress={onPress}
                        style={styles.buttonStyle}>
                        <Text style={styles.buttonText}>{'Download'}</Text>
                    </TouchableOpacity>
                    <AddHeight size={5} />
                </View>
            </View>
            <AddHeight size={30} />
        </View>
    )
}

MarketingCollateralItem.defaultProps = {
    item: {
        id: undefined,
        title: undefined,
        url: undefined
    },
    index: 0,
    screen: ROUTES.MARKETING_COLLATERAL_PAGE,
    onPress: (item, index) => { },
}

export default MarketingCollateralItem;