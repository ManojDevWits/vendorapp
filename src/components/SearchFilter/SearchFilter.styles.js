import { StyleSheet,Platform } from "react-native"
import { Colors, FontFamily, fontScale, scale } from "../../theme";

const isIos = Platform.OS === 'ios';
const shadow = {
    backgroundColor: Colors.white,
    borderRadius: scale(5),
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 0,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4,
    elevation: 3,
}

const styles = StyleSheet.create({
    label: {
        fontSize: fontScale(18),
        fontFamily: FontFamily.Regular,
        color: Colors.infoLabel
    },
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    inputContainer: {
        ...shadow,
        flex: 1,
        height:scale(40), 
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: scale(15),
    },
    textInput: {
        flex: 1,
        borderBottomColor: 'transparent',
        fontSize: fontScale(15),
        fontFamily: FontFamily.Regular,
        color: Colors.black,
        padding:0,
    },
    buttonContainer: {
        ...shadow,
    },
    button: {
        height: scale(40),
        width: scale(40),
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputHeight:{
        height:scale(50)
    }
})

export default styles;