import React, { useEffect, useState } from 'react'
import { View, Platform, Image, TextInput, TouchableOpacity, Keyboard } from 'react-native'
import { Colors, scale } from '../../theme';
import styles from './SearchFilter.styles';
import SearchIcon from '../../assets/icons/ic_search_grey.svg'
import FilterIcon from '../../assets/icons/ic_filter.svg'
import ClearIcon from '../../assets/icons/ic_close_black.svg'
import { AddWidth } from '../AddSpace/AddSpace';

const SearchFilter = ({ onChangeText, onPressFilter, onSubmit, onSearchClear }) => {
    const [text, setText] = useState('')
    return (
        <View>
            <View
                style={
                    styles.container
                }>
                <View
                    style={
                        styles.inputContainer
                    }>
                    <SearchIcon />
                    <AddWidth size={10} />
                    <TextInput
                        style={
                            styles.textInput
                        }
                        value={text}
                        numberOfLines={1}
                        selectionColor={Colors.primary}
                        placeholder="Search Product Name..."
                        placeholderTextColor={Colors.inputLabel}
                        onChangeText={text => {
                            setText(prev => text)
                            onChangeText(text)
                        }}
                        returnKeyType={'search'}
                        onSubmitEditing={() => { Keyboard.dismiss; onSubmit(text) }}
                    />
                    <AddWidth size={10} />
                    <TouchableOpacity onPress={() => {
                        setText(prev => '')
                        onChangeText('')
                        onSearchClear()
                    }}>
                        <ClearIcon />
                    </TouchableOpacity>

                </View>
                <AddWidth size={12} />
                <View style={
                    styles.buttonContainer
                }>
                    <TouchableOpacity
                        onPress={onPressFilter}
                        style={styles.button}>
                        <FilterIcon width={scale(26)} height={scale(26)} />
                    </TouchableOpacity>
                </View>

            </View>
        </View>
    )
}

SearchFilter.defaultProps = {
    onPressFilter: () => { },
    onChangeText: (text) => { },
    onSubmit: (text) => { },
    onSearchClear: () => { }
}

export default SearchFilter;