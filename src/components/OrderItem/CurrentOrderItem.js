import React, {useCallback} from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { Colors, FontFamily, fontScale, scale } from '../../theme';
import styles from './CurrentOrderItem.styles';
import ROUTES from '../../navigators/Routes'
import { AddWidth, AddHeight } from '../AddSpace/AddSpace'
import TimeGreyIcon from '../../assets/icons/ic_time_grey.svg'
import { Constant } from '../../constants/constant';
import ButtonLeftIcon from '../ButtonLeftIcon/ButtonLeftIcon';
import Amount from '../Amount/Amount';
import moment from 'moment';


const CurrentOrderItem = ({ item, index, screen, onPressEdit, onPressTime }) => {

    const onEdit = useCallback(
        () => {
            onPressEdit(item, index)
        },
        [item, onPressEdit],
    )

    const getOrderStatusColor = () => {
        switch (item.status) {
            case Constant.OrderStatus.PENDING:
                return styles.pendingText;
            case Constant.OrderStatus.DELAYED:
                return styles.delayedText;
            case Constant.OrderStatus.COMPLETE:
                return styles.completedText;
            case Constant.OrderStatus.CANCELLED:
                return styles.canceledText;
        }
    }

    const getDateToShow = (date) => {
        if (date && date != null && date != "") {
            const dateType = moment(date, Constant.DateFormat.DD_MM_YYYY);
            return moment(dateType).format(Constant.DateFormat.DD_MMM_YYYY).toString();
        } else {
            return 'NA'
        }
    }

    const renderOrderStatus = () => {
        return (
            <View style={
                [styles.orderRowContainer, styles.row]
            }>
                <View style={
                    styles.row
                }>
                    <View>
                        <Text
                            style={
                                styles.normalLabel
                            }
                        >{'Order Id : ' + item?.orderID}</Text>
                    </View>
                    <View style={
                        styles.pipe
                    } />
                    <View style={
                        styles.row
                    }>
                        <Text style={
                            styles.normalLabel
                        }>{'Status : '}</Text>
                        <Text
                            style={
                                [styles.normalLabel, getOrderStatusColor(), {}]
                            }
                        >{item?.orderStatus}</Text>
                    </View>
                </View>
                <ButtonLeftIcon
                    label={'Edit'}
                    onPress={onPressEdit} />
            </View >
        )
    }

    const renderTopView = () => {
        return (
            <View>
                <View style={[styles.row,]}>
                    {
                        item?.productimage &&  item?.productimage != "" && item?.productimage != "0"?
                        <Image
                            source={{ uri: item?.productimage }}
                            style={styles.image}
                        />
                        :<View style={styles.imagePlaceHolder}/>
                    }

                    <AddWidth size={10} />
                    <View style={{ flex: 1 }}>
                        <View style={styles.titleContainer}>
                            <Text style={styles.title}>{item?.productqty + ' X ' + item?.productname}</Text>
                            <Text style={styles.normalLabel}>{getDateToShow(item?.orderDate)}</Text>
                            {/* <Amount amount={item.amount}/> */}
                        </View>
                        <AddHeight size={3} />
                        <View style={styles.titleContainer}>
                            <Text style={styles.normalLabel}>{'Order Id : ' + item?.orderID}</Text>
                            <ButtonLeftIcon
                                label={'Edit'}
                                onPress={onEdit} />
                        </View>
                        <AddHeight size={3} />
                        <View style={styles.titleContainer}>
                            <Text style={
                                styles.normalLabel
                            }>{'Status : '}
                                <Text
                                    style={
                                        [styles.normalLabel, styles.pendingText, {}]
                                    }
                                >{item?.orderStatus}</Text>
                            </Text>

                        </View>
                    </View>
                </View>
                {/* {renderOrderStatus()} */}

            </View>
        )
    }

    const getTime = (type) => {
        let time = null;
        switch (type) {
            case Constant.TimeType.ESTIMATED:
                time = item.estimatedTime;
                break;
            case Constant.TimeType.ACKNOWLEDGEMENT:
                time = item.acknowledgementTime;
                break;
            case Constant.TimeType.PICKUP:
                time = item.pickupTime;
                break;
        }

        return time;
    }

    return (
        <View>
            <TouchableOpacity
                onPress={onEdit}
                activeOpacity={0.9}
                style={[styles.container]}>
                {renderTopView()}
            </TouchableOpacity>
            <AddHeight size={15} />
        </View>
    )
}

CurrentOrderItem.defaultProps = {
    item: {
        orderID: undefined,
        sale_item_id: undefined,
        productname: undefined,
        productimage: undefined,
        productqty: 0,
        orderStatus: undefined,
        orderDate: undefined,
        actual_pickup_date_time: null,
        acknowledgment_date_time: null,
    },
    index: 0,
    screen: ROUTES.CURRENT_ORDERS_PAGE,
    onPressEdit: (item, index) => { },
    onPressTime: () => { }
}

export default CurrentOrderItem;