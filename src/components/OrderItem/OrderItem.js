import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { Colors, FontFamily, fontScale, scale } from '../../theme';
import styles from './OrderItem.styles';
import ROUTES from '../../navigators/Routes'
import { AddWidth, AddHeight } from '../AddSpace/AddSpace'
import TimeGreyIcon from '../../assets/icons/ic_time_grey.svg'
import { Constant } from '../../constants/constant';
import ButtonLeftIcon from '../ButtonLeftIcon/ButtonLeftIcon';
import Amount from '../Amount/Amount';


const OrderItem = ({ item, index, screen, onPressEdit, onPressTime }) => {

    const getOrderStatusColor = () => {
        switch (item.status) {
            case Constant.OrderStatus.PENDING:
                return styles.pendingText;
            case Constant.OrderStatus.DELAYED:
                return styles.delayedText;
            case Constant.OrderStatus.COMPLETE:
                return styles.completedText;
            case Constant.OrderStatus.CANCELLED:
                return styles.canceledText;
        }
    }

    const renderOrderStatus = () => {
        return (
            <View style={
                [styles.orderRowContainer, styles.row]
            }>
                <View style={
                    styles.row
                }>
                    <View>
                        <Text
                            style={
                                styles.normalLabel
                            }
                        >{'Order Id : ' + item.orderId}</Text>
                    </View>
                    <View style={
                        styles.pipe
                    } />
                    <View style={
                        styles.row
                    }>
                        <Text style={
                            styles.normalLabel
                        }>{'Status : '}</Text>
                        <Text
                            style={
                                [styles.normalLabel, getOrderStatusColor(), {}]
                            }
                        >{item.status}</Text>
                    </View>
                </View>
                <ButtonLeftIcon
                    label={'Edit'}
                    onPress={onPressEdit} />
            </View >
        )
    }

    const renderTopView = () => {
        return (
            <View style={
                {
                    padding: scale(10),
                }
            }>
                <View style={[styles.row,]}>
                    <Image
                        source={{ uri: item.image }}
                        style={{ height: scale(50), width: scale(50), resizeMode: 'cover', borderRadius: 5 }}
                    />
                    <AddWidth size={10} />
                    <View style={{ flex: 1 }}>
                        <View style={styles.titleContainer}>
                            <Text style={styles.title}>{item.name}</Text>
                            <Amount amount={item.amount}/>
                        </View>
                        <View style={styles.titleContainer}>
                            <Text style={styles.normalLabel}>{'Quantity : ' + item.quantity}</Text>
                            <Text style={styles.normalLabel}>{item.date}</Text>
                        </View>
                    </View>
                </View>
                {renderOrderStatus()}

            </View>
        )
    }

    const getTime = (type) => {
        let time = null;
        switch (type) {
            case Constant.TimeType.ESTIMATED:
                time = item.estimatedTime;
                break;
            case Constant.TimeType.ACKNOWLEDGEMENT:
                time = item.acknowledgementTime;
                break;
            case Constant.TimeType.PICKUP:
                time = item.pickupTime;
                break;
        }

        return time;
    }

    const renderBottomItem = (type) => {
        return (
            <TouchableOpacity style={
                styles.bottomItem
            }>
                <View style={
                    styles.bottomItemLabelContainer
                }>
                    <Text
                        style={
                            styles.normalLabel
                        }
                    >{type}</Text>
                    <AddWidth size={4} />
                    <TimeGreyIcon width={scale(20)} height={scale(20)} />
                </View>
                <Text style={
                    styles.bottomTime
                }>{getTime(type)}</Text>
            </TouchableOpacity>
        )
    }

    const renderBottomView = () => {
        return (
            <View style={
                styles.bottomItemContainer
            }>
                {renderBottomItem(Constant.TimeType.ESTIMATED)}
                <AddWidth scale={4} />
                {renderBottomItem(Constant.TimeType.ACKNOWLEDGEMENT)}
                <AddWidth scale={4} />
                {renderBottomItem(Constant.TimeType.PICKUP)}
            </View>
        )
    }

    return (
        <View>
            <TouchableOpacity
                // onPress={() => { }}
                activeOpacity={0.9}
                style={[styles.container, { borderColor: item.color, }]}>
                {renderTopView()}
                <View style={{
                    height: 2,
                    backgroundColor: Colors.itemDivider
                }} />
                {renderBottomView()}
            </TouchableOpacity>
            <AddHeight size={25} />
        </View>
    )
}

OrderItem.defaultProps = {
    item: {
        orderId: 'ABXD43244',
        name: "Paneer Tikka",
        image: 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png',
        quantity: 2,
        amount: 237,
        status: 'Pending',
        date: '17 May 2021 at 6:30 PM',
        estimatedTime: '10:20',
        acknowledgementTime: '10:20',
        pickupTime: '10:20',
        color: 'red'

    },
    index: 0,
    screen: ROUTES.CURRENT_ORDERS_PAGE,
    onPressEdit: (item,index) => { },
    onPressTime: () => { }
}

export default OrderItem;