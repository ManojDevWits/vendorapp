import React, { useCallback } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { Colors, FontFamily, fontScale, scale } from '../../theme';
import styles from './OrderHistoryItem.styles';
import ROUTES from '../../navigators/Routes'
import { AddWidth, AddHeight } from '../AddSpace/AddSpace'
import TimeGreyIcon from '../../assets/icons/ic_time_grey.svg'
import { Constant } from '../../constants/constant';
import ButtonLeftIcon from '../ButtonLeftIcon/ButtonLeftIcon';
import Amount from '../Amount/Amount';
import moment from 'moment';


const OrderHistoryItem = ({ item, index, screen, onPressEdit, onPressTime }) => {

    const onEdit = useCallback(
        () => {
            onPressEdit(item, index)
        },
        [item, onPressEdit],
    )

    const getDateTimeToShow = (date) => {
        if (date && date != null && date != "" && !isNaN(date)) {
            return moment(date * 1000, 'x').format(Constant.DateFormat.DD_MMM_YYYY_HH_MM);
            // return moment(dateType).format(Constant.DateFormat.DD_MMM_YYYY).toString();
        } else {
            return 'Invalid Date'
        }
    }

    const RenderDateTimeView = ({ label, dateTime }) => {
        return (
            <View style={{ alignItems: 'flex-end' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    <Text style={[styles.normalLabel]}>{label}</Text>
                    <AddWidth size={4} />
                    <TimeGreyIcon width={scale(20)} height={scale(20)} />
                </View>
                <Text style={[
                    styles.normalLabel,
                    {
                        fontSize: fontScale(11),
                        color: Colors.primary,
                        alignSelf: 'flex-end',
                    }]}>{getDateTimeToShow(dateTime)}</Text>
            </View>
        )
    }

    const renderTopView = () => {
        return (
            <View>
                <View style={[styles.row,]}>
                    {
                        item?.productimage && item?.productimage != "" && item?.productimage != "0" ?
                            <Image
                                source={{ uri: item?.productimage }}
                                style={styles.image}
                            />
                            : <View style={styles.imagePlaceHolder} />
                    }

                    <AddWidth size={10} />
                    <View style={{ flex: 1 }}>
                        <View style={styles.titleContainer}>
                            <Text style={styles.title}>{item?.productqty + ' X ' + item?.productname}</Text>
                            <Text style={styles.normalLabel}>{'Order Id : ' + item?.orderID}</Text>
                        </View>
                        <AddHeight size={3} />
                        {/* <View style={styles.titleContainer}> */}
                        {/* <Text style={styles.normalLabel}>{'Order Id : ' + item?.orderID}</Text> */}
                        {/* <ButtonLeftIcon
                                label={'Edit'}
                                onPress={onEdit} /> */}
                        {/* </View> */}
                        <AddHeight size={3} />
                        <View style={styles.titleContainer}>
                            <RenderDateTimeView label={"Acknowledgement"} dateTime={item?.acknowledgment_date_time} />
                            <RenderDateTimeView label={"Actual Pickup"} dateTime={item?.actual_pickup_date_time} />
                        </View>
                    </View>
                </View>
                {/* {renderOrderStatus()} */}

            </View>
        )
    }

    return (
        <View>
            <TouchableOpacity
                onPress={onEdit}
                activeOpacity={0.9}
                style={[styles.container, screen === ROUTES.ORDER_DELAYED_PAGE ? styles.delayBorder : {}]}>
                {renderTopView()}
            </TouchableOpacity>
            <AddHeight size={15} />
        </View>
    )
}

OrderHistoryItem.defaultProps = {
    item: {
        orderID: undefined,
        sale_item_id: undefined,
        productname: undefined,
        productimage: undefined,
        productqty: 0,
        orderStatus: undefined,
        orderDate: undefined,
        actual_pickup_date_time: null,
        acknowledgment_date_time: null,
    },
    index: 0,
    screen: ROUTES.CURRENT_ORDERS_PAGE,
    onPressEdit: (item, index) => { },
    onPressTime: () => { }
}

export default OrderHistoryItem;