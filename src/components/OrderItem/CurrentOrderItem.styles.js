import { StyleSheet } from "react-native"
import { Colors, FontFamily, fontScale, scale } from "../../theme";

const shadow = {
    backgroundColor: Colors.white,
    borderRadius: scale(10),
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 0,
    },
    shadowOpacity: 0.1,
    shadowRadius: 10,
    elevation: 3,
}

const styles = StyleSheet.create({
    container: {
        ...shadow,
        // height: scale(120),
        backgroundColor: Colors.white,
        borderRadius: scale(5),
        borderWidth: 0.5,
        borderColor: Colors.gainsboro
        // flexDirection: 'row',
    },
    row: {
        flexDirection: 'row',
        margin:scale(12)
    },
    image: {
        height: scale(50),
        width: scale(50),
        resizeMode: 'cover',
        borderRadius: 5
    },
    imagePlaceHolder:{
        height: scale(50),
        width: scale(50),
        borderRadius: 5,
        backgroundColor:Colors.dateRange
    },
    bottomTime: {
        color: Colors.primary,
        fontFamily: FontFamily.Bold,
        fontSize: fontScale(12)
    },
    normalLabel: {
        fontFamily: FontFamily.Regular,
        color: Colors.black,
        fontSize: fontScale(12),
    },
    pendingText: {
        color: Colors.pendingOrders,
        fontFamily: FontFamily.Medium
    },
    delayedText: {
        color: Colors.delayedOrders,
        fontFamily: FontFamily.Medium
    },
    completedText: {
        color: Colors.completedOrders,
        fontFamily: FontFamily.Medium
    },
    canceledText: {
        color: Colors.canceledOrders,
        fontFamily: FontFamily.Medium
    },
    orderRowContainer: {
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    pipe: {
        marginHorizontal: scale(8),
        width: 1,
        height: scale(18),
        backgroundColor: Colors.black
    },
    titleContainer: {
        flex: 1,
        flexDirection: 'row',
        // alignItems: 'center',
        justifyContent: 'space-between'
    },
    title: {
        color: Colors.black,
        fontFamily: FontFamily.Bold,
        fontSize: fontScale(13),
        flex: 1
    }
})

export default styles;