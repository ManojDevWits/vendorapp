import React from 'react'
import Modal from 'react-native-modal';

export default ModalComponent = ({
    style,
    onSwipeComplete,
    onBackdropPress,
    children,
    isVisible,
    onClose,
    ...props
}) => {
    return (
        <Modal
            style={style}
            onSwipeComplete={onSwipeComplete || onClose}
            isVisible={isVisible}
            onBackButtonPress={onBackdropPress || onClose}
            animationInTiming={400}
            animationOutTiming={400}
            backdropTransitionInTiming={400}
            backdropTransitionOutTiming={400}
            useNativeDriver
            hideModalContentWhileAnimating
            {...props}
        >
            {children}
        </Modal>
    )
}
