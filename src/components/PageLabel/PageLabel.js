import React from 'react'
import { View, Text, Image } from 'react-native'
import { Colors } from '../../theme';
import styles from './PageLabel.styles';

const PageLabel = ({ label = "" }) => {
    return (
        <View>
            <Text style={
                styles.label
            }>{label}</Text>
            <View style={
                styles.bottomHighlighter
            } />
        </View>
    )
}

export default PageLabel;