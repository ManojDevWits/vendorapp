import { StyleSheet } from "react-native"
import { Colors, FontFamily, fontScale, scale } from "../../theme";


const styles = StyleSheet.create({
    label: {
        fontSize: fontScale(20),
        fontFamily: FontFamily.SemiBold,
        color: Colors.black,
    },
    bottomHighlighter: {
        width: scale(32),
        height: scale(5),
        marginTop: scale(3),
        backgroundColor: Colors.bottomHighLighter,
        borderRadius: scale(5),
    }
})

export default styles;