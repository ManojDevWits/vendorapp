import { StyleSheet, Platform } from "react-native"
import { Colors, scale, fon, fontScale, FontFamily } from "../../theme";

const isIos = Platform.OS === 'ios';
const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        borderWidth:2,
        borderColor:Colors.button,
        borderRadius:3,
        backgroundColor: Colors.button,
    },
    button: {
        backgroundColor: Colors.button,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        // paddingVertical:scale(1),
        paddingHorizontal:scale(4),
    },
    label:{
        color:Colors.white,
        fontFamily:FontFamily.Medium,
        fontSize:fontScale(14),
        height:scale(16),
        lineHeight:scale(isIos?18:19),
    }
})

export default styles;