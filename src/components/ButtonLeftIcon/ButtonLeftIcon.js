import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { Colors, scale } from '../../theme';
import styles from './ButtonLeftIcon.styles';
import DefaultIcon from '../../assets/icons/ic_edit_white.svg'
import { AddWidth } from '../AddSpace/AddSpace';

const ButtonLeftIcon = ({
    label,
    Icon,
    iconHeight,
    iconWidth,
    onPress,
    containerStyle,
    buttonStyle,
    labelStyle,
}) => {
    return (
        <View style={[styles.container, containerStyle]}>
            <TouchableOpacity
                activeOpacity={0.7}
                style={
                    [styles.button, buttonStyle]
                }
                onPress={onPress}>

                <Icon width={iconWidth} height={iconHeight} />
                <AddWidth size={5} />
                <Text style={[styles.label, labelStyle]}>{label}</Text>

            </TouchableOpacity>
        </View>
    )
}

ButtonLeftIcon.defaultProps = {
    label: 'Edit',
    Icon: DefaultIcon,
    containerStyle: {},
    buttonStyle: {},
    labelStyle: {},
    iconWidth: 11,
    iconHeight: 11,
    onPress: () => { }
}

export default ButtonLeftIcon;