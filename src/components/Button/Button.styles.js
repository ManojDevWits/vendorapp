import { StyleSheet, Platform } from "react-native"
import { Colors, scale, fon, fontScale, FontFamily } from "../../theme";

const isIos = Platform.OS === 'ios';
const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
    },
    button: {
        backgroundColor: Colors.button,
        borderRadius: scale(50),
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.5,
        shadowRadius: 10,
        shadowColor: Colors.button,
        elevation: 5,
    },
    buttonItemContainer: {
        justifyContent: 'center',
    },
    labelContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        height: scale(isIos ? 56 : 62)
    },
    label: {
        fontSize: fontScale(20),
        fontFamily: FontFamily.Medium,
        color: Colors.white,
        // padding:15,
    },
    rightIconContainer: {
        position: 'absolute',
        right: scale(isIos ? 5 : 6),
        transform: [{ rotate: '180deg' }]
    }
})

export default styles;