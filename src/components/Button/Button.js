import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { Colors, scale } from '../../theme';
import styles from './Button.styles';
import Arrow from '../../assets/icons/ic_button_arrow.svg'

const Button = ({ label = "", onPress = () => {} }) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity
                activeOpacity={0.7}
                style={
                    styles.button
                }
                onPress={onPress}>
                <View style={styles.buttonItemContainer}>
                    <View style={styles.labelContainer}>
                        <Text style={
                            styles.label
                        }>{label}</Text>
                    </View>

                    <View style={
                        styles.rightIconContainer
                    }>
                        <Arrow width={scale(48)} height={scale(48)}/>
                    </View>
                </View>

            </TouchableOpacity>
        </View>
    )
}

export default Button;