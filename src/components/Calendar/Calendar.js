import React, { useCallback, useState } from 'react'
import { View, Platform, Text, Image, TouchableOpacity, Modal } from 'react-native'
import styles from './Calendar.styles'
import CalendarPicker from 'react-native-calendar-picker';
import { Colors, FontFamily, fontScale, scale } from '../../theme';
import moment from 'moment';

// import ModalComponent from '../ModalComponent/ModalComponent'

const dateFormatToPass = "DD-MM-YYYY";
export default Calendar = ({
    visible,
    onClose,
    onDateSelected,
    onDateRangeSelected,
    allowRange = false,
    date,
    startDate,
    endDate
}) => {
    const getDateFromString = (date) => {
        if (date && date != null && date != "") {
            return moment(date, dateFormatToPass)
        } else {
            return null
        }
    }

    const getStringFromDate = (date) => {
        if (date && date != null && !isNaN(date) && date != "") {
            return moment(date).format(dateFormatToPass).toString();
        } else {
            return null;
        }
    }

    const [selectedDate, setSelectedDate] = useState(getDateFromString(date));
    const [selectedStartDate, setSelectedStartDate] = useState(getDateFromString(startDate));
    const [selectedEndDate, setSelectedEndDate] = useState(getDateFromString(endDate));

    const onConfirm = useCallback(
        () => {
            console.log("DATE_CONFIRM" + selectedDate)
            console.log("START_DATE_CONFIRM" + selectedStartDate)
            console.log("END_DATE_CONFIRM" + selectedEndDate)
            if (allowRange) {
                if (selectedEndDate == null) {
                    onDateRangeSelected(getStringFromDate(selectedStartDate), getStringFromDate(selectedStartDate))
                } else {
                    onDateRangeSelected(getStringFromDate(selectedStartDate), getStringFromDate(selectedEndDate))
                }
            } else {
                onDateSelected(getStringFromDate(selectedDate))
            }
        },
        [selectedDate, selectedStartDate, selectedEndDate],
    )

    const onDateChange = useCallback(
        (date, type) => {
            if (allowRange) {
                if (type === 'START_DATE') {
                    setSelectedStartDate(date)
                }
                if (type === 'END_DATE') {
                    setSelectedEndDate(date)
                }
            } else {
                setSelectedDate(date)
            }
        },
        [selectedDate, selectedStartDate, selectedEndDate],
    )

    return (
        <Modal
            // transparent={true}
            visible={visible}
            onBackdropPress={onClose}
            onRequestClose={onClose}
        >
            <TouchableOpacity
                style={styles.container}
                onPress={onClose}
            >
                <TouchableOpacity
                    onPress={onDateChange}
                    activeOpacity={1}
                    style={styles.calendarContainer}>
                    <CalendarPicker
                        onDateChange={onDateChange}
                        startFromMonday={true}
                        width={scale(320)}
                        height={scale(420)}
                        allowRangeSelection={allowRange}
                        allowBackwardRangeSelect={true}
                        todayBackgroundColor={Colors.dateRange}
                        todayTextStyle={{ color: Colors.black }}
                        selectedDayColor={Colors.primary}
                        selectedDayTextColor={Colors.white}
                        selectedRangeStartTextStyle={styles.selectedRangeStartTextStyle}
                        selectedRangeEndTextStyle={styles.selectedRangeEndTextStyle}
                        selectedRangeStartStyle={styles.selectedRangeStartStyle}
                        selectedRangeStyle={styles.selectedRangeStyle}
                        selectedRangeEndStyle={styles.selectedRangeEndStyle}
                        textStyle={styles.textStyle}
                        selectedStartDate={selectedStartDate || selectedDate}
                        selectedEndDate={selectedEndDate}
                    />
                    <TouchableOpacity
                        style={styles.confirmButton}
                        onPress={onConfirm}
                    >
                        <Text style={styles.confirmText}>CONFIRM</Text>
                    </TouchableOpacity>
                </TouchableOpacity>

            </TouchableOpacity>
        </Modal>
    )
}
