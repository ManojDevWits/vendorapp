import React, { useCallback, useState, useRef } from 'react'
import { View, Platform, Text, Image, TouchableOpacity, Modal } from 'react-native'
import styles from './TimePicker.styles'
import CalendarPicker from 'react-native-calendar-picker';
import { Colors, FontFamily, fontScale, scale } from '../../theme';
import moment from 'moment';
import DateTimePickerModal from "react-native-modal-datetime-picker";


const dateFormatToPass = "hh : mm A";
export default TimePicker = ({
    visible,
    onClose,
    value,
    onTimeSelected,
}) => {
    const getStringFromTime = (date) => {
        if (date && date != null && date != "") {
            return moment(date).format(dateFormatToPass).toString();
        } else {
            return null;
        }
    }

    const onPickupTimeSelected = useCallback(
        (time) => {
            onTimeSelected(getStringFromTime(time))
        },
        [visible, onTimeSelected],
    )

    const renderTimerConfirmButton = ({ onPress }) => {
        return (
            <View>
                <View style={{
                    height: scale(1),
                    backgroundColor: Colors.itemDivider
                }} />
                <TouchableOpacity
                    onPress={onPress}
                    activeOpacity={1}
                    style={{
                        flexDirection: 'row',
                        padding: scale(10),
                        backgroundColor: Colors.white,
                        height: scale(57),
                    }}>
                    <View style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignSelf: 'center',
                    }}>
                        <Text style={{
                            color: Colors.primary,
                            fontSize: fontScale(19),
                            alignSelf: 'center',
                            fontFamily: FontFamily.Regular
                        }}>{'Confirm'}</Text>
                    </View>
                </TouchableOpacity>
            </View>

        )
    }

    const renderTimerCancelButton = ({ }) => {
        return (
            <TouchableOpacity
                onPress={onClose}
                activeOpacity={1}
                style={{
                    flexDirection: 'row',
                    padding: scale(10),
                    backgroundColor: Colors.white,
                    marginBottom: scale(20),
                    height: scale(60),
                    borderRadius: scale(14),
                }}>
                <View style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignSelf: 'center',
                }}>
                    <Text style={{
                        color: Colors.primary,
                        fontSize: fontScale(19),
                        alignSelf: 'center',
                        fontFamily: FontFamily.Medium
                    }}>{'Cancel'}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <Modal
            visible={visible}
            onBackdropPress={onClose}
            onRequestClose={onClose}
        >
            <TouchableOpacity
                style={styles.container}
                onPress={onClose}
            >
                <DateTimePickerModal
                    isVisible={visible}
                    mode="time"
                    onConfirm={onPickupTimeSelected}
                    onCancel={onClose}
                    customConfirmButtonIOS={renderTimerConfirmButton}
                    customCancelButtonIOS={renderTimerCancelButton}
                />
            </TouchableOpacity>
        </Modal>
    )
}
