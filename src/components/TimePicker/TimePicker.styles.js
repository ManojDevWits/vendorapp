import { StyleSheet, Platform,} from 'react-native'
import { Colors, FontFamily, fontScale, scale } from "../../theme";

const isIos = Platform.OS == 'ios'
const headerSize = 56;

const shadow = {
    backgroundColor: Colors.white,
    borderRadius: scale(5),
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 0,
    },
    shadowOpacity: 0.2,
    shadowRadius: 6,
    elevation: 3,
}
const styles = StyleSheet.create({
    container:{
        ...StyleSheet.absoluteFill,
        backgroundColor:Colors.calendarBackground,
        alignItems:'center',
        justifyContent:'center'
    },
    calendarContainer:{
        ...shadow,
        alignSelf:'center',
        backgroundColor:Colors.white,
        borderRadius:10,
    },
    selectedRangeStartTextStyle:{
        color:Colors.white
    },
    selectedRangeEndTextStyle:{
        color:Colors.white
    },
    selectedRangeStartStyle:{
        backgroundColor: Colors.primary,
        borderTopLeftRadius: scale(50),
        borderBottomLeftRadius: scale(50),
    },
    selectedRangeStyle:{
        backgroundColor: Colors.dateRange
    },
    selectedRangeEndStyle:{
        backgroundColor: Colors.primary,
        borderTopRightRadius: scale(50),
        borderBottomRightRadius: scale(50),
    },
    textStyle:{
        fontFamily: FontFamily.Regular,
        color: Colors.black,
    },
    confirmButton:{
        alignSelf: 'flex-end',
        margin: scale(20),
        backgroundColor: Colors.button,
        borderRadius: scale(50),
    },
    confirmText:{
        fontSize: fontScale(12),
        color: Colors.white,
        fontFamily: FontFamily.SemiBold,
        marginVertical: scale(8),
        marginHorizontal: scale(20)
    }
})

export default styles;