import { StyleSheet } from "react-native"
import { Colors, FontFamily, fontScale, scale } from "../../theme";


const styles = StyleSheet.create({
    container: {
        borderRadius: scale(20),
    },
    draggableIcon:{
        backgroundColor: Colors.draggableIcon,
        width: scale(100),
        height:scale(4)
    }
})

export default styles;