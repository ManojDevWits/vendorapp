import React, { useRef, useEffect } from 'react'
import { View, Text, Image } from 'react-native'
import { Colors, scale } from '../../theme';
import styles from './BottomSheet.styles';
import RBSheet from "react-native-raw-bottom-sheet";

const BottomSheet = ({
    isVisible,
    children,
    onClose,
    container,
    modal
}) => {
    const sheetRef = useRef(null)

    useEffect(() => {
        if (sheetRef) {
            if (isVisible) {
                sheetRef.current.open()
            } else {
                sheetRef.current.close()
            }
        }
    }, [isVisible])

    return (
        <View>
            <RBSheet
                ref={sheetRef}
                openDuration={250}
                customStyles={{
                    container: { ...styles.container, ...container },
                    draggableIcon: styles.draggableIcon
                }}
                closeOnDragDown={true}
                onClose={onClose}
                keyboardAvoidingViewEnabled={true}
            >
                {children}

            </RBSheet>
            {/* {modal && modal()} */}
        </View>

    )
}

BottomSheet.defaultProps = {
    isVisible: false,
    children: null,
    close: () => { },
    container: {},
    modal:() => { }
}


export default BottomSheet;