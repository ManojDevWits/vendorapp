import React from 'react'
import { View, Text, Image } from 'react-native'
import { scale } from '../../theme'

export const AddHeight = ({ size = 0 }) => {
    return (
        <View style={{ height:scale(size) }} />
    )
}


export const AddWidth = ({ size = 0 }) => {
    return (
        <View style={{ width:scale(size) }} />
    )
}
