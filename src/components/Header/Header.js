import React from 'react'
import { View, Platform, Text, Image, TouchableOpacity } from 'react-native'
import { Colors, scale } from '../../theme'
import HamburgerIcon from '../../assets/icons/ic_burger_menu.svg'
import logo from '../../assets/images/img_logo.png'
import styles from './Header.styles'
import NavigationService from '../../service/NavigationService'
import { useNavigation } from '@react-navigation/native';


export default Header = () => {
    const navigation = useNavigation();

    return (
        <View>
            <View style={
                styles.card
            } >
                <Image source={logo}
                    style={styles.logo} />
                <TouchableOpacity
                    style={
                        styles.menu
                    }
                    onPress={() => { navigation.openDrawer() }}
                >
                    <HamburgerIcon />
                </TouchableOpacity>

            </View>
        </View>

    )
}
