import { StyleSheet, Platform,} from 'react-native'
import { Colors, FontFamily, fontScale, scale } from "../../theme";

const isIos = Platform.OS == 'ios'
const headerSize = 56;

const styles = StyleSheet.create({
    card:{
        height: headerSize,
        backgroundColor: Colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.1,
        shadowRadius: 4,
        elevation: 5,
    },
    logo:{
        height: scale(isIos ? 44 : 48),
        resizeMode: 'contain'
    },
    menu:{
        position: 'absolute',
        left: scale(10),
        width: scale(50),
        height: headerSize,
        alignItems: 'center',
        justifyContent: 'center',
    }
})

export default styles;