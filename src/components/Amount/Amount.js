import React from 'react'
import { View, Text, Image } from 'react-native'
import { Colors } from '../../theme';
import styles from './Amount.styles';
import DefaultIcon from '../../assets/icons/ic_rupee_indian.svg'

const Amount = ({ amount, textStyle }) => {
    return (
        <View style={
            {
                flexDirection: 'row',
                alignItems:'center'
            }
        }>
            <DefaultIcon height={11} width={11} />
            <Text style={   
                [styles.amount, textStyle]
            }>{amount}</Text>
        </View>
    )
}

Amount.defaultProps = {
    amount: '0.00',
    Icon: DefaultIcon,
    textStyle: styles.amount,
}

export default Amount;