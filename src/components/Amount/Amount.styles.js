import { StyleSheet, Platform } from "react-native"
import { Colors, FontFamily, fontScale, scale } from "../../theme";

const styles = StyleSheet.create({
    amount: {
        color: Colors.black,
        fontFamily: FontFamily.Bold,
        fontSize: fontScale(14),
        height: scale(15),
        lineHeight: scale(16),
    }
})

export default styles;