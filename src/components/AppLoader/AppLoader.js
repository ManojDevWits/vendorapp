import React from 'react'
import { View, ActivityIndicator, Text, Image, TouchableOpacity } from 'react-native'
import { Colors, scale } from '../../theme'
import HamburgerIcon from '../../assets/icons/ic_burger_menu.svg'
import styles from './AppLoader.styles'
import NavigationService from '../../service/NavigationService'
import { useNavigation } from '@react-navigation/native';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';


const AppLoader = ({ visible, color, size }) => {
    const { isLoading } = useSelector((state) => ({
        isLoading: state.commonReducer.isLoading,
    }), shallowEqual);
    return (
        isLoading || visible
            ? <View style={styles.container}>
                <ActivityIndicator size={size} color={color} />
            </View>
            : null
    )
}

AppLoader.defaultProps = {
    visible: false,
    color: Colors.primary,
    size: "large"
}


export default AppLoader;