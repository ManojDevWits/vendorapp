import { StyleSheet, Platform, } from 'react-native'
import { Colors, FontFamily, fontScale, scale } from "../../theme";

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:Colors.loaderBackground
    },
})

export default styles;