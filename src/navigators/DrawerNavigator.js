import React, { useCallback, useEffect, useState } from 'react';
import { createDrawerNavigator, DrawerItem } from "@react-navigation/drawer";
import DashboardPage from "../containers/DashboardPage/DashboardPage";
import NavigationDrawer from "../containers/NavigationDrawer/NavigationDrawer";
import CurrentOrdersPage from '../containers/Orders/CurrentOrdersPage/CurrentOrdersPage';
import ROUTES from './Routes';
import { Colors } from '../theme';
import SplashScreen from 'react-native-splash-screen'
import MarketingCollateralPage from '../containers/MarketingCollateralPage/MarketingCollateralPage';
import ComplianceDocumentPage from '../containers/ComplianceDocumentPage/ComplianceDocumentPage';
import OrderHistoryPage from '../containers/Orders/OrderHistoryPage/OrderHistoryPage';
import CancelledOrdersPage from '../containers/Orders/CancelledOrdersPage/CancelledOrdersPage';
import OrderDelayedPage from '../containers/Orders/OrderDelayedPage/OrderDelayedPage';


const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
    useEffect(() => {
        SplashScreen.hide();
    }, [])
    return (
        <Drawer.Navigator
            drawerStyle={{ width: '80%' }}
            drawerContent={(props) => <NavigationDrawer {...props} />
            } >
            <Drawer.Screen name={ROUTES.DASHBOARD_PAGE} component={DashboardPage} />
            <Drawer.Screen name={ROUTES.CURRENT_ORDERS_PAGE} component={CurrentOrdersPage} />
            <Drawer.Screen name={ROUTES.ORDER_HISTORY_PAGE} component={OrderHistoryPage} />
            <Drawer.Screen name={ROUTES.CANCELLED_ORDERS_PAGE} component={CancelledOrdersPage} />
            <Drawer.Screen name={ROUTES.ORDER_DELAYED_PAGE} component={OrderDelayedPage} />
            <Drawer.Screen name={ROUTES.MARKETING_COLLATERAL_PAGE} component={MarketingCollateralPage} />
            <Drawer.Screen name={ROUTES.COMPLIANCE_DOCUMENT_PAGE} component={ComplianceDocumentPage} />

        </Drawer.Navigator>
    );
}

export default DrawerNavigator;