import { NavigationContainer } from "@react-navigation/native";
import {
    SafeAreaView,
    StatusBar
  } from 'react-native';
import React, { useCallback, useEffect, useState } from 'react';
import SplashPage from "../containers/SplashPage/SplashPage";
import { navigationRef } from '../service/NavigationService'
import { Colors } from "../theme";
import DashboardStack from "./DashboardStack";
import DrawerNavigator from "./DrawerNavigator";
import AuthStack from "./AuthStack";
import AppLoader from "../components/AppLoader/AppLoader";
import { shallowEqual, useDispatch, useSelector } from 'react-redux';


const MyNavigationContainer = () => {
    // const [isSplash, setSplash] = useState(false)
    // const [isSessionAvailable, setSession] = useState(false)
    const { isSplash,isSessionAvailable } = useSelector((state) => ({
        isSplash: state.commonReducer.isSplash,
        isSessionAvailable: state.commonReducer.isSessionAvailable,
    }), shallowEqual);


    // if (isSplash) return <SplashPage />
    return (
        <NavigationContainer ref={navigationRef} >
            <>
                {/* <SafeAreaView style={{ backgroundColor: Colors.statusbar }} /> */}
                {/* <StatusBar barStyle="dark-content" backgroundColor={Colors.statusbar} /> */}
                {/* <DrawerNavigator /> */}
                {isSplash? <SplashPage /> :isSessionAvailable ? <DrawerNavigator /> : <AuthStack />}
                {/* {isSessionAvailable ? <DrawerNavigator /> : <AuthStack />} */}
                <AppLoader/>
            </>
        </NavigationContainer>
    );
}

export default MyNavigationContainer;