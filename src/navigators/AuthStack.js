import React, { useCallback, useEffect, useState } from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import LoginPage from "../containers/LoginPage/LoginPage"
import OtpPage from "../containers/OtpPage/OtpPage"
import ROUTES from './Routes';

const Stack = createStackNavigator()

const AuthStack = () => (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name={ROUTES.LOGIN_PAGE} component={LoginPage} />
        <Stack.Screen name={ROUTES.OTP_PAGE} component={OtpPage} />
        {/* <Stack.Screen name={ROUTES.OTP_PAGE} component={OtpPage} initialParams={{ mobile: '9898989898' }} /> */}
    </Stack.Navigator>
)

export default AuthStack