import AsyncStorage from "@react-native-async-storage/async-storage";
import { applyMiddleware, combineReducers, createStore, Store } from "redux";
import { Persistor, persistReducer, persistStore } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import rootSaga  from '../src/redux/sagas';
import rootReducer from '../src/redux/reducers'

const sagaMiddleware = createSagaMiddleware();
const persistConfig = {
    // Root
    key: 'root',
    // Storage Method (React Native)
    storage: AsyncStorage,
    // Whitelist (Save Specific Reducers)
    whitelist: [
        "dataReducer",
    ],
    blacklist: [],
    throttle: 1000,
    debounce: 1000,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

let store = createStore(
    persistedReducer,/* preloadedState, */
    applyMiddleware(sagaMiddleware)
)

// Middleware: Redux Persist Persister
let persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export { store, persistor };

