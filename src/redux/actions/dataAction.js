import * as ActionType from '../actionType'

export const updateTokenAction = (token) => {
    return {
        type: ActionType.UPDATE_TOKEN,
        payload: token
    };
}

export const updateUserDataAction = (userData) => {
    return {
        type: ActionType.UPDATE_USER_DATA,
        payload: userData
    };
}