import * as ActionType from '../actionType'

export const updateSplashAction = (splashStatus) => {
    return {
        type: ActionType.UPDATE_SPLASH,
        payload: splashStatus
    };
}

export const updateLoaderAction = (visible) => {
    return {
        type: ActionType.UPDATE_LOADING,
        payload: visible
    };
}

export const updateLoginSessionAction = (isSessionAvailable) => {
    return {
        type: ActionType.UPDATE_LOGIN_SESSION,
        payload: isSessionAvailable
    };
}