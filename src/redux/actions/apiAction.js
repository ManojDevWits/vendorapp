import * as ActionType from '../actionType'

export const callApiSaga = ({
    screen = null,
    apiType = null,
    params = null,
    request = null,
    showAppLoader = true,
    successCallback = (res) => { },
    failureCallback = (res) => { }
}) => {
    return {
        type: ActionType.CALL_API_SAGA,
        params: params,
        request: request,
        screen: screen,
        apiType: apiType,
        showAppLoader:showAppLoader,
        successCallback: successCallback,
        failureCallback: failureCallback
    };
}