
import { put, call, takeLatest, select } from "redux-saga/effects"
import { config } from "../../api"
import Api from "../../api/api"
import { Constant } from "../../constants"
import { showToast } from "../../utils/Toast"
import * as actionType from "../actionType"

export const apiTimer = 15000;
function* apiSaga() {
    yield takeLatest(actionType.CALL_API_SAGA, callApiSaga)
}

//action will contain below items
// {
//     type: ActionType.CALL_API_SAGA,
//     params: params,
//     request: request,
//     screen: screen,
//     apiType: apiType,
//     showAppLoader = showAppLoader,
//     successCallback: successCallback,
//     failureCallback: failureCallback
// }
function* callApiSaga(action) {
    const successCallback = action.successCallback;
    const failureCallback = action.failureCallback;
    try {
        yield put({
            type: actionType.UPDATE_LOADING,
            payload: action.showAppLoader
        })
        let response;
        switch (action.apiType) {
            case Constant.ApiType.VERIFY_TOKEN:
                const state = yield select();
                const authToken = state.dataReducer.token;
                response = yield call(verifyToken, action, authToken)
                break;
            case Constant.ApiType.SEND_OTP:
                response = yield call(sendOtp, action)
                break;
            case Constant.ApiType.RESEND_OTP:
                response = yield call(resendOtp, action)
                break;
            case Constant.ApiType.VERIFY_MOBILE:
                response = yield call(verifyMobile, action)
                break;
            case Constant.ApiType.DASHBOARD:
                response = yield call(dashboard, action)
                break;
            case Constant.ApiType.MY_ORDERS:
                response = yield call(myOrders, action)
                break;
            case Constant.ApiType.HISTORY_ORDERS:
                response = yield call(historyOrders, action)
                break;
            case Constant.ApiType.ORDER_DELAYED:
                response = yield call(orderDelayed, action)
                break;
            case Constant.ApiType.UPDATE_ACK_DATE_TIME:
                response = yield call(updateAckDateTime, action)
                break;
            case Constant.ApiType.UPDATE_PICKUP_DATE_TIME:
                response = yield call(updatePickupDateTime, action)
                break;
        }

        yield put({
            type: actionType.UPDATE_LOADING,
            payload: false
        })
        if (response && response.status && response.status === 200 && response.data) {
            // console.log("RESPONSE" + JSON.stringify(response))
            const { data } = response;
           
            if (data.success) {
                successCallback(data)
            } else {
                failureCallback(data)
                if (data?.message) {
                    throw Error(data.message)
                }
            }
        } else {
            throw Error('Api request failed')
        }
        // const response = yield call(checkApi, action)
        // console.log("API_RESPONSE")
        // console.log(JSON.stringify(response))
    } catch (error) {
        console.log("ERROR:")
        console.log(error)
        yield put({
            type: actionType.UPDATE_LOADING,
            payload: false
        })
        failureCallback(error)
        if (error && error.message) {
            showToast(error.message);
        } else {
            showToast("Something went wrong");
        }
    }
}

export default apiSaga


// function sendOtp(action) {
//     return Api.post(config.endpoint.sendOtp, null, action?.params, 15000);
// }

function verifyToken(action, token) {
    // return Api.get("http://dummy.restapiexample.com/api/v1/employee/1", null,null, 15000);
    return Api.get(config.endpoint.verifyToken, { 'token': token }, null, apiTimer);
}

function sendOtp(action) {
    // return Api.get("http://dummy.restapiexample.com/api/v1/employee/1", null,null, 15000);
    return Api.post(config.endpoint.sendOtp, null, null, action?.request, apiTimer);
}

function resendOtp(action) {
    return Api.post(config.endpoint.resendOtp, null, null, action?.request, apiTimer);
}

function verifyMobile(action) {
    return Api.post(config.endpoint.verifyMobile, null, null, action?.request, apiTimer);
}

function dashboard(action) {
    return Api.post(config.endpoint.dashboard, null, null, action?.request, apiTimer);
}

function myOrders(action) {
    return Api.post(config.endpoint.myOrders, null, null, action?.request, apiTimer);
}

function historyOrders(action) {
    return Api.post(config.endpoint.historyOrders, null, null, action?.request, apiTimer);
}

function orderDelayed(action) {
    return Api.post(config.endpoint.orderDelayed, null, null, action?.request, apiTimer);
}

function updateAckDateTime(action) {
    return Api.post(config.endpoint.updateAckDateTime, null, null, action?.request, apiTimer);
}

function updatePickupDateTime(action) {
    return Api.post(config.endpoint.updatePickupDateTime, null, null, action?.request, apiTimer);
}