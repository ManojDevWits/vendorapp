import * as actionType from "../actionType";

const initialDataState = {
    token: undefined,
    userData: {
        name:undefined,
    },
}

const initialState = {
    ...initialDataState,
}

const dataReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionType.UPDATE_TOKEN:
            return {
                ...state,
                token: action.payload
            }
        case actionType.UPDATE_USER_DATA:
            return {
                ...state,
                userData: action.payload
            }
        default:
            return state;
    }
}

export default dataReducer;