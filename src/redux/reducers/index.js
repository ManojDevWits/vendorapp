import { combineReducers } from "redux"
import dataReducer from "./dataReducer"
import commonReducer from "./commonReducer"

export default combineReducers(
    {
        dataReducer,
        commonReducer
    }
);