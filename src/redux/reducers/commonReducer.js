import * as actionType from "../actionType";

const initialCommonState = {
    isSplash: true,
    isLoading: false,
    isSessionAvailable: false,
}

const initialState = {
    ...initialCommonState,
}

const commonReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionType.UPDATE_SPLASH:
            return {
                ...state,
                isSplash: action.payload
            };
        case actionType.UPDATE_LOADING:
            return {
                ...state,
                isLoading: action.payload
            };
        case actionType.UPDATE_LOGIN_SESSION: return {
            ...state,
            isSessionAvailable: action.payload
        };
        default:
            return state;
    }
}

export default commonReducer;