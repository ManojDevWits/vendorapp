import Toast from 'react-native-root-toast';
import { Colors } from '../theme';

export const showToast = message => {
    return Toast.show(message)
}