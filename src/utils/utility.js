

export const getOrderApiRequestForm = ({
    limit = 20,
    start = 1,
    startDate,
    endDate,
    orderId,
    productName,
    status
}) => {
    let formData = new FormData();
    formData.append('limit', limit);
    formData.append('start', start);
    if (startDate && startDate != null) {
        formData.append('start_date', startDate);
    }
    if (endDate && endDate != null) {
        formData.append('end_date', endDate);
    }
    if (orderId && orderId != null) {
        formData.append('order_id', orderId);
    }
    if (productName && productName != null) {
        formData.append('product_name', productName);
    }
    if (status && status != null) {
        formData.append('status', status);
    }

    return formData;
}